"""Contains ImpactSurveyBot class."""
from typing import Any, Dict

import requests

from bot.workspace_handler import WorkspaceHandler
from database.database import Postgres
from logger.logger import Logger
from request.update import UpdateRequest
from storage.s3 import S3
from structs.context import BotContext
from structs.messages import IncommingMessage, ResponseMessage


class ImpactSurveyBot:
    """Singleton to orchestrate bot functionality on root level."""

    def __init__(self, config: Dict[str, Any]) -> None:
        self.__config = config

        self.logger: Logger = Logger(self.__config["logging"])

        self.__postgres_db: Postgres = Postgres(
            self.__config["postgres"],
            self.logger,
        )
        self.__s3_client: S3 = S3(self.__config["s3"])

        self.__instance_id = int(self.__config["instance"]["instance_id"])
        self.__timeout_interval = int(self.__config["timeout"]["timeout_interval"])

        self.__wab_phone_number_id = self.__config["whatsapp_business"][
            "phone_number_id"
        ]

        self.__bot_context: BotContext = BotContext(
            logger=self.logger,
            postgres=self.__postgres_db,
            s3=self.__s3_client,
            timeout_interval=self.__timeout_interval,
            wab_api_token=self.__config["whatsapp_business"]["api_token"],
            telegram_api_token=self.__config["telegram"]["api_token"],
        )

        self.__workspace_handler = WorkspaceHandler(
            bot_ctx=self.__bot_context, instance_id=self.__instance_id
        )

    def __del__(self) -> None:
        self.__postgres_db.close()

    def process(self, request: IncommingMessage) -> ResponseMessage:
        """Process incoming twilio request and return respective bot response."""
        resp = self.__workspace_handler.process(request)
        return resp

    def update(self, request: UpdateRequest) -> None:
        """Triggers update of workspace defined by request."""
        self.__workspace_handler.update(request)

    def send_whatsapp(
        self, recipient_phone_number: str, response: ResponseMessage
    ) -> int:
        """Sends whatsapp message to phonenumber."""
        phone_number_id = self.__wab_phone_number_id
        api_token = self.__bot_context.wab_api_token

        url = f"https://graph.facebook.com/v15.0/{phone_number_id}/messages"
        headers = {
            "Authorization": f"Bearer {api_token}",
            "Content-Type": "application/json",
        }

        res = requests.post(
            url,
            headers=headers,
            json=response.whatsapp_business(recipient_phone_number),
        )
        return res.status_code

    def send_telegram(self, identifier: str, response: ResponseMessage) -> int:
        """Sends telegram message to telegram user."""
        api_token = self.__bot_context.telegram_api_token

        url = f"https://api.telegram.org/bot{api_token}/sendMessage"
        if response.image:
            url = f"https://api.telegram.org/bot{api_token}/sendPhoto"

        res = requests.post(url, json=response.telegram(identifier))
        return res.status_code
