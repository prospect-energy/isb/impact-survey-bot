from unittest import TestCase

from database import migration


class TestMigration(TestCase):
    def test_translate_column_types(self):
        column_dict = {
            "primary_key": "integer",
            "column1": "text-100",
            "column2": "text-long",
            "column3": "integer",
            "column4": "decimal",
            "column5": "number",
            "column6": "location",
            "column7": "picture",
            "column8": "date",
            "column9": "time",
            "column10": "time-12",
            "column11": "time-24",
            "column12": "url",
            "column13": "phonenumber",
        }

        expected = {
            "column1": "character varying(100)",
            "column2": "text",
            "column3": "integer",
            "column4": "numeric",
            "column5": "numeric",
            "column6": "numeric",
            "column7": "text",
            "column8": "date",
            "column9": "time without time zone",
            "column10": "time without time zone",
            "column11": "time without time zone",
            "column12": "text",
            "column13": "character varying(50)",
        }

        res = migration.translate_column_types(column_dict, "primary_key")
        self.assertDictEqual(res, expected)

    def test_get_length_insensitive_column_dict(self):
        sql_column_dict = {
            "column1": "character varying(100)",
            "column2": "text",
            "column3": "integer",
            "column4": "date",
            "column5": "time without time zone",
            "column6": "character varying(50)",
        }

        expected = {
            "column1": "character varying",
            "column2": "text",
            "column3": "integer",
            "column4": "date",
            "column5": "time without time zone",
            "column6": "character varying",
        }

        res = migration.get_length_insensitive_column_dict(sql_column_dict)
        self.assertDictEqual(res, expected)
