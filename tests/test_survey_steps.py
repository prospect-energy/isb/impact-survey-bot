from unittest import TestCase

from result.status import NotSet
from session.session import Session
from structs.context import SessionContext
from structs.messages import ResponseMessage, TranslatedMessage
from structs.sections import ConditionSection
from structs.selection import SelectionOption
from survey import steps


class MockContext:
    default_language = "en"


class MockWorkspace:
    ctx = MockContext()


class TestSteps(TestCase):
    def test_condition_fulfilled(self):
        session = Session(MockWorkspace(), "testing_identifier")
        session_ctx = SessionContext(
            None, None, None, 0, "", "", 0, "", "", 0, "", None, session
        )

        def test_func(ctx: SessionContext, param: str):
            if param == "test":
                return False

            return True

        res = steps.condition_fulfilled(session_ctx, None)
        self.assertTrue(res, "Because condition is None it should return True.")

        condition = ConditionSection(test_func, {"param": "test"}, {}, None)

        res = steps.condition_fulfilled(session_ctx, condition)
        self.assertFalse(res, "Condition is expected to return False.")

        session.survey_variables["var.test"] = "test"
        condition = ConditionSection(
            test_func,
            {"param": "{{var.test}}"},
            {"param": ["{{var.test}}", "var.test"]},
            None,
        )

        res = steps.condition_fulfilled(session_ctx, condition)
        self.assertFalse(res, "Condition with Variable is expected to return False.")
        session.survey_variables["var.test"] = NotSet()

        condition = ConditionSection(test_func, {"param": "heureka"}, {}, None)

        res = steps.condition_fulfilled(session_ctx, condition)
        self.assertTrue(res, "Condition is expected to return True.")

    def test_resolve_variables_in_params(self):
        session = Session(MockWorkspace(), "testing_identifier")
        session_ctx = SessionContext(
            None, None, None, 0, "", "", 0, "", "", 0, "", None, session
        )

        session.variables.clear()
        session.variables["var.test"] = "some_value"

        params = {"param1": "{{var.test}}", "param2": "{{var.invalid}}"}
        variables = {
            "param1": ["{{var.test}}", "var.test"],
        }
        expected = {"param1": "some_value", "param2": "{{var.invalid}}"}

        res = steps.resolve_variables_in_params(session_ctx, params, variables)
        self.assertDictEqual(
            res, expected, "Only the valid variable should be replaced."
        )

        session.variables.clear()

        params = {
            "param": "{{var.test}}",
        }
        variables = {
            "param": ["{{var.test}}", "var.test"],
        }

        res = steps.resolve_variables_in_params(session_ctx, params, variables)
        self.assertTrue(
            isinstance(res["param"], NotSet), "Variable is not set privously."
        )

        session.variables.clear()
        session.variables["var.test"] = "some_value"

        params = {
            "dict_param": {"param": "{{var.test}}"},
        }
        variables = {
            "dict_param": {"param": ["{{var.test}}", "var.test"]},
        }
        expected = {
            "dict_param": {"param": "some_value"},
        }

        res = steps.resolve_variables_in_params(session_ctx, params, variables)
        self.assertDictEqual(
            res, expected, "Items of dictionary param should be resolved as well."
        )

    def test_build_selection_response(self):
        session = Session(MockWorkspace(), "testing_identifier")
        session_ctx = SessionContext(
            None, None, None, 0, "", "", 0, "", "", 0, "", None, session
        )

        session.groups = ["group1", "default"]
        session.language = "en"
        session.variables = {"var.test": "some_value"}

        translated_message = TranslatedMessage(
            {"en": "This is a message with {{ var.test }}."}
        )
        translated_message.variables["{{ var.test }}"] = "var.test"
        options = {
            "group1": [
                SelectionOption(
                    TranslatedMessage({"en": "Option1"}), "", False, "some_successor"
                )
            ],
            "group2": [
                SelectionOption(
                    TranslatedMessage({"en": "Option2"}), "", False, "some_successor"
                )
            ],
            "default": [],
        }
        image = "http://images.abc/test.png"

        expected_options = ["Option1"]

        res = steps.build_selection_response(
            session_ctx, translated_message, options, image
        )
        self.assertTrue(
            isinstance(res, ResponseMessage),
            "Function should return ResponseMessage object.",
        )
        self.assertEqual(res.type, "selection")
        self.assertEqual(res.message, "This is a message with some_value.")
        self.assertEqual(res.image, image)
        self.assertListEqual(res.options, expected_options)
        self.assertEqual(
            res.option_error, "", "option_error should be set by session_handler."
        )

    def test_build_message_response(self):
        session = Session(MockWorkspace(), "testing_identifier")
        session_ctx = SessionContext(
            None, None, None, 0, "", "", 0, "", "", 0, "", None, session
        )

        session.language = "en"
        session.variables = {"var.test": "some_value"}

        translated_message = TranslatedMessage(
            {"en": "This is a message with {{ var.test }}."}
        )
        translated_message.variables["{{ var.test }}"] = "var.test"
        image = "http://images.abc/test.png"

        res = steps.build_message_response(session_ctx, translated_message, image)
        self.assertTrue(
            isinstance(res, ResponseMessage),
            "Function should return ResponseMessage object.",
        )
        self.assertEqual(res.type, "message")
        self.assertEqual(res.message, "This is a message with some_value.")
        self.assertEqual(res.image, image)
        self.assertListEqual(
            res.options, [], "Message response should not have any options."
        )
        self.assertEqual(
            res.option_error, "", "option_error should be set by session_handler."
        )

        res = steps.build_message_response(session_ctx, None, image)
        self.assertIsNone(res, "If message is None, function should return None.")
