from unittest import TestCase

from parsing import modifier
from structs.messages import IncommingMessage


class TestModifier(TestCase):
    def test_to_lower_str(self):
        val = "TeST"
        expected = "test"
        res = modifier.to_lower_str(val)
        self.assertEqual(res, expected, f"Expected {expected} as return.")

    def test_to_lower(self):
        msg = IncommingMessage("", "TeST", None, None, None)
        expected = "test"
        res = modifier.to_lower(msg)
        self.assertEqual(
            res.message, expected, f"Expected {expected} as request message."
        )

    def test_to_upper_str(self):
        val = "tEst"
        expected = "TEST"
        res = modifier.to_upper_str(val)
        self.assertEqual(res, expected, f"Expected {expected} as return.")

    def test_to_upper(self):
        msg = IncommingMessage("", "tEst", None, None, None)
        expected = "TEST"
        res = modifier.to_upper(msg)
        self.assertEqual(
            res.message, expected, f"Expected {expected} as request message."
        )

    def test_remove_spaces_str(self):
        val = "T es t"
        expected = "Test"
        res = modifier.remove_spaces_str(val)
        self.assertEqual(res, expected, f"Expected {expected} as return.")

    def test_remove_spaces(self):
        msg = IncommingMessage("", "T es t", None, None, None)
        expected = "Test"
        res = modifier.remove_spaces(msg)
        self.assertEqual(
            res.message, expected, f"Expected {expected} as request message."
        )

    def test_comma_to_dot(self):
        msg = IncommingMessage("", "3,14,5,2", None, None, None)
        expected = "3.14.5.2"
        res = modifier.comma_to_dot(msg)
        self.assertEqual(
            res.message, expected, f"Expected {expected} as request message."
        )

    def test_remove_units_str(self):
        val = "3.14 mm^2"
        expected = "3.14"
        res = modifier.remove_units_str(val)
        self.assertEqual(res, expected, f"Expected {expected} as return.")

    def test_remove_units(self):
        msg = IncommingMessage("", "3.14 mm^2", None, None, None)
        expected = "3.14"
        res = modifier.remove_units(msg)
        self.assertEqual(
            res.message, expected, f"Expected {expected} as request message."
        )
