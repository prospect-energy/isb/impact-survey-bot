from unittest import TestCase

from type_check import media


class TestMedia(TestCase):
    def test_has_file_type(self):
        val = "Selection.JSoN"
        res = media.has_file_type(["json"], val)
        self.assertTrue(res, f"{str(val)} is a json file name")

        val = "http://images.abc/test.png"
        res = media.has_file_type(["mp4", "avi", "mov"], val)
        self.assertFalse(res, f"{str(val)} isn't one of the required file types")

        val = "some_file"
        res = media.has_file_type(["txt", "docx"], val)
        self.assertFalse(res, f"{str(val)} hasn't a file type")

    def test_is_json_file(self):
        val = "Selection.json"
        res = media.is_json_file(val)
        self.assertTrue(res, f"{str(val)} is a json file name")

        val = "Selection"
        res = media.is_json_file(val)
        self.assertFalse(res, f"{str(val)} isn't a json file name")

        val = "Selection.ini"
        res = media.is_json_file(val)
        self.assertFalse(res, f"{str(val)} isn't a json file name")

    def test_is_ini_file(self):
        val = "config.ini"
        res = media.is_ini_file(val)
        self.assertTrue(res, f"{str(val)} is an ini file name")

        val = "config"
        res = media.is_ini_file(val)
        self.assertFalse(res, f"{str(val)} isn't an ini file name")

        val = "Selection.json"
        res = media.is_ini_file(val)
        self.assertFalse(res, f"{str(val)} isn't an ini file name")
