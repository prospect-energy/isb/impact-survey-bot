from typing import Any, Dict, List, Optional, Tuple, Union
from unittest import TestCase

from parsing import survey_components
from result.error import ComponentException, InvalidTypeException, MandatoryException


class TestSurveyComponents(TestCase):
    def test_is_optional(self):
        type_ = tuple
        res = survey_components.is_optional(type_)
        self.assertFalse(res)

        type_ = Optional[Dict[str, str]]
        res = survey_components.is_optional(type_)
        self.assertTrue(res)

        type_ = Optional[Union[str, int]]
        res = survey_components.is_optional(type_)
        self.assertTrue(res)

    def test_get_optional_type(self):
        type_ = Optional[str]
        res = survey_components.get_optional_type(type_)
        self.assertEqual(res, str)

        type_ = Optional[Union[Dict[str, Any], str]]
        res = survey_components.get_optional_type(type_)
        self.assertEqual(res.__args__, (Dict[str, Any], str))

        type_ = Optional[Tuple[str, int, Any]]
        res = survey_components.get_optional_type(type_)
        self.assertEqual(res, Tuple[str, int, Any])

    def test_is_compatible_type(self):
        value = "some_string"
        expected_type = str
        res = survey_components.is_compatible_type(value, expected_type)
        self.assertTrue(res)

        value = None
        expected_type = Optional[Dict[str, str]]
        res = survey_components.is_compatible_type(value, expected_type)
        self.assertFalse(res, "Function ignores optional flag.")

        value = {"some_key": "some_value"}
        expected_type = Dict[str, str]
        res = survey_components.is_compatible_type(value, expected_type)
        self.assertTrue(res)

        value = 42
        expected_type = Optional[Union[str, float, int]]
        res = survey_components.is_compatible_type(value, expected_type)
        self.assertTrue(res)

        value = [1, 2, 3]
        expected_type = list
        res = survey_components.is_compatible_type(value, expected_type)
        self.assertTrue(res)

        value = 3.14
        expected_type = Any
        res = survey_components.is_compatible_type(value, expected_type)
        self.assertTrue(res)

    def test_verify_tuple(self):
        key = "some_key_name"
        value = ["some_value", 42, 3.14, None]
        expected_type = Tuple[str, int, float, Any]
        res = survey_components.verify_tuple(key, value, expected_type)
        self.assertIsNone(res)

        key = "some_key_name"
        value = ["some_value", 42]
        expected_type = Tuple[str, int, Optional[float], Optional[Any]]
        res = survey_components.verify_tuple(key, value, expected_type)
        self.assertIsNone(res)

        key = "some_key_name"
        value = ["some_value", "not_an_integer"]
        expected_type = Tuple[str, int, Optional[float], Optional[Any]]
        with self.assertRaises(InvalidTypeException):
            survey_components.verify_tuple(key, value, expected_type)

        key = "some_key_name"
        value = ["some_value", 42]
        expected_type = Tuple[str, int, float, Optional[Any]]
        with self.assertRaises(InvalidTypeException):
            survey_components.verify_tuple(key, value, expected_type)

    def test_verify_list(self):
        key = "some_key_name"
        value = [1, 2, 3]
        expected_type = List[int]
        res = survey_components.verify_list(key, value, expected_type)
        self.assertIsNone(res)

        key = "some_key_name"
        value = [["Option1", "some_successor"], ["Option2", "some_successor"]]
        expected_type = List[Tuple[str, str]]
        res = survey_components.verify_list(key, value, expected_type)
        self.assertIsNone(res)

        key = "some_key_name"
        value = 42
        expected_type = List[int]
        with self.assertRaises(InvalidTypeException):
            survey_components.verify_list(key, value, expected_type)

        key = "some_key_name"
        value = [1, 2, "3", 4]
        expected_type = List[int]
        with self.assertRaises(InvalidTypeException):
            survey_components.verify_list(key, value, expected_type)

    def test_get_type_checked_value(self):
        key = "some_key_name"
        value = [1, 2, 3]
        expected_type = List[int]
        res = survey_components.get_type_checked_value(key, value, expected_type)
        self.assertEqual(res, value)

        key = "some_key_name"
        value = ["some_string", 42]
        expected_type = Optional[Tuple[str, int]]
        res = survey_components.get_type_checked_value(key, value, expected_type)
        self.assertEqual(res, value)

        key = "some_key_name"
        value = 3.14
        expected_type = float
        res = survey_components.get_type_checked_value(key, value, expected_type)
        self.assertEqual(res, value)

    def test_parse_survey_component(self):
        with self.assertRaises(ComponentException):
            survey_components.parse_survey_component({}, "not_a_dict")

        interface = {"key1": int, "key2": List[str], "key3": Optional[float]}
        json_dict = {"key1": 42, "key2": ["string1", "string2"]}
        res = survey_components.parse_survey_component(interface, json_dict)
        self.assertEqual(res, json_dict)

        interface = {"key1": int, "key2": List[str]}
        json_dict = {"key1": 42}
        with self.assertRaises(MandatoryException):
            survey_components.parse_survey_component(interface, json_dict)

        class TestComponent(survey_components.Component):
            def __init__(self, json_dict: dict) -> None:
                interface = {"answer": int, "pi": float}

                json_dict = survey_components.parse_survey_component(
                    interface, json_dict
                )

                self.answer = json_dict.get("answer", None)
                self.pi = json_dict.get("pi", None)

        interface = {"name": str, "data": TestComponent}
        json_dict = {"name": "some_name", "data": {"answer": 42, "pi": 3.14}}
        res = survey_components.parse_survey_component(interface, json_dict)
        self.assertEqual(res["name"], "some_name")
        self.assertTrue(isinstance(res["data"], TestComponent))
        self.assertEqual(res["data"].answer, 42)
        self.assertEqual(res["data"].pi, 3.14)
