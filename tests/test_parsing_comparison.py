from datetime import datetime
from unittest import TestCase

from parsing import comparison


class TestComparison(TestCase):
    def test_less(self):
        val = 4.2
        tgt = 5
        res = comparison.less(val, tgt)
        self.assertTrue(res, f"{str(val)} is less than {str(tgt)}")

        val = datetime.strptime("18:20", "%H:%M")
        tgt = datetime.strptime("16:05", "%H:%M")
        res = comparison.less(val, tgt)
        self.assertFalse(res, f"{str(val)} is not less than {str(tgt)}")

        val = 1.34
        tgt = ["5.3"]
        res = comparison.less(val, tgt)
        self.assertFalse(res, f"{str(val)} or {str(tgt)} have unsuported types")

    def test_less_or_equal(self):
        val = 4
        tgt = 5
        res = comparison.less_or_equal(val, tgt)
        self.assertTrue(res, f"{str(val)} is less than {str(tgt)}")

        val = 3.2
        tgt = 3.2
        res = comparison.less_or_equal(val, tgt)
        self.assertTrue(res, f"{str(val)} is equal {str(tgt)}")

        val = datetime.strptime("18:20", "%H:%M")
        tgt = datetime.strptime("16:05", "%H:%M")
        res = comparison.less_or_equal(val, tgt)
        self.assertFalse(res, f"{str(val)} is not less or equal than {str(tgt)}")

        val = 1.34
        tgt = ["5.3"]
        res = comparison.less_or_equal(val, tgt)
        self.assertFalse(res, f"{str(val)} or {str(tgt)} have unsuported types")

    def test_equal(self):
        val = ["4"]
        tgt = ["4"]
        res = comparison.equal(val, tgt)
        self.assertTrue(res, f"{str(val)} is equal {str(tgt)}")

        val = 5.3
        tgt = 1
        res = comparison.equal(val, tgt)
        self.assertFalse(res, f"{str(val)} is not equal {str(tgt)}")

        val = 1.34
        tgt = ["5.3"]
        res = comparison.equal(val, tgt)
        self.assertFalse(res, f"{str(val)} or {str(tgt)} have unsuported types")

    def test_greater_or_equal(self):
        val = 5
        tgt = 4
        res = comparison.greater_or_equal(val, tgt)
        self.assertTrue(res, f"{str(val)} is greater than {str(tgt)}")

        val = 3.2
        tgt = 3.2
        res = comparison.greater_or_equal(val, tgt)
        self.assertTrue(res, f"{str(val)} is equal {str(tgt)}")

        val = datetime.strptime("16:05", "%H:%M")
        tgt = datetime.strptime("18:20", "%H:%M")
        res = comparison.greater_or_equal(val, tgt)
        self.assertFalse(res, f"{str(val)} is not greater or equal than {str(tgt)}")

        val = 1.34
        tgt = ["5.3"]
        res = comparison.greater_or_equal(val, tgt)
        self.assertFalse(res, f"{str(val)} or {str(tgt)} have unsuported types")

    def test_between(self):
        val = 5
        lower = 4.2
        upper = "100"
        res = comparison.between(val, lower, upper)
        self.assertTrue(res, f"{str(val)} is between {str(lower)} and {str(upper)}")

        val = 4.2
        lower = 4.2
        upper = "100"
        res = comparison.between(val, lower, upper)
        self.assertFalse(
            res, f"{str(val)} is not between {str(lower)} and {str(upper)}"
        )

        val = datetime.strptime("16:05", "%H:%M")
        lower = datetime.strptime("10:14", "%H:%M")
        upper = datetime.strptime("14:20", "%H:%M")
        res = comparison.between(val, lower, upper)
        self.assertFalse(
            res, f"{str(val)} is not between {str(lower)} and {str(upper)}"
        )

        val = 1.34
        lower = ["2"]
        upper = "5.3"
        res = comparison.between(val, lower, upper)
        self.assertFalse(
            res, f"{str(val)}, {str(lower)}, {str(upper)} have unsuported types"
        )

    def test_between_including(self):
        val = 4.2
        lower = 4.2
        upper = "100"
        res = comparison.between_including(val, lower, upper)
        self.assertTrue(
            res, f"{str(val)} is between or equal to {str(lower)} and {str(upper)}"
        )

        val = 66.2
        lower = 4.2
        upper = "100"
        res = comparison.between_including(val, lower, upper)
        self.assertTrue(
            res, f"{str(val)} is between or equal to {str(lower)} and {str(upper)}"
        )

        val = datetime.strptime("16:05", "%H:%M")
        lower = datetime.strptime("10:14", "%H:%M")
        upper = datetime.strptime("14:20", "%H:%M")
        res = comparison.between_including(val, lower, upper)
        self.assertFalse(
            res, f"{str(val)} is not between or equal to {str(lower)} and {str(upper)}"
        )

        val = 1.34
        lower = ["2"]
        upper = "5.3"
        res = comparison.between_including(val, lower, upper)
        self.assertFalse(
            res, f"{str(val)}, {str(lower)}, {str(upper)} have unsuported types"
        )

    def test_greater_midnight(self):
        val = datetime.strptime("18:20", "%H:%M")
        tgt = datetime.strptime("16:05", "%H:%M")
        res = comparison.greater_midnight(val, tgt)
        self.assertTrue(res, f"{str(val)} is greater than {str(tgt)}")

        val = datetime.strptime("2:20", "%H:%M")
        tgt = datetime.strptime("23:02", "%H:%M")
        res = comparison.greater_midnight(val, tgt)
        self.assertTrue(res, f"{str(val)} is greater than {str(tgt)}")

        val = datetime.strptime("5:32", "%H:%M")
        tgt = datetime.strptime("21:12", "%H:%M")
        res = comparison.greater_midnight(val, tgt)
        self.assertFalse(res, f"{str(val)} is not greater than {str(tgt)}")

        val = 1.34
        tgt = 5
        res = comparison.greater_or_equal(val, tgt)
        self.assertFalse(res, f"{str(val)} and {str(tgt)} are not datetimes")

    def test_get_typed_value(self):
        val = "4.2"
        expected = 4.2
        res = comparison.get_typed_value(val)
        self.assertEqual(res, expected, f"{str(val)} can be cast to float")

        val = "15/03/2020"
        expected = datetime.strptime(val, "%d/%m/%Y")
        res = comparison.get_typed_value(val)
        self.assertEqual(res, expected, f"{str(val)} can be cast to date")

        val = "5:32"
        expected = datetime.strptime(val, "%H:%M")
        res = comparison.get_typed_value(val)
        self.assertEqual(res, expected, f"{str(val)} can be cast to time")

        val = ["abc"]
        expected = val
        res = comparison.get_typed_value(val)
        self.assertEqual(res, expected, f"{str(val)} can't be cast")
