# Module: Type_Check

This module contains various functions for type-checking different values. In Particular, participant inputs need to be type-checked and validated for completeness.

## When to use it?

You should use this module when you want to check whether a `IncommingMessage` value is not empty or has the right data type. Additionally, the module contains functions for validating the format of strings (e.g. datetime, url, phone_number ...)
