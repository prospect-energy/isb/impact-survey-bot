"""Contains functions for url and filename validation."""
from typing import List

from structs.messages import Media, MediaType


def is_image_file(media: Media) -> bool:
    """Returns whether media file is an image type."""
    mime_types = ["image/jpeg", "image/png"]

    return is_media_type(media, MediaType.IMAGE, mime_types)


def is_audio_file(media: Media) -> bool:
    """Returns whether media file is an audio type."""
    mime_types = [
        "audio/ogg",
        "video/ogg",
        "audio/mpeg",
        "audio/aac",
        "audio/mp4",
        "audio/amr",
    ]

    return is_media_type(media, MediaType.AUDIO, mime_types)


def is_video_file(media: Media) -> bool:
    """Returns whether media file is a video type."""
    mime_types = ["video/mp4", "video/3sp"]

    return is_media_type(media, MediaType.VIDEO, mime_types)


def is_document_file(media: Media) -> bool:
    """Returns whether media file is a document type."""
    mime_types = [
        "application/pdf",
        "text/plain",
        "application/msword",
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "application/vnd.ms-excel",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        "application/vnd.ms-powerpoint",
        "application/vnd.openxmlformats-officedocument.presentationml.presentation",
    ]

    return is_media_type(media, MediaType.DOCUMENT, mime_types)


def is_media_type(
    media: Media, required_type: MediaType, mime_types: List[str]
) -> bool:
    """Returns whether media object is not None and fits the required MediaType or mime-types."""
    if not media:
        return False

    if media.media_type:
        return media.media_type == required_type.value

    return media.content_type in mime_types


def is_json_file(filename: str) -> bool:
    """Returns whether filename indicates json file.

    Required suffix: .json
    """
    return has_file_type(["json"], filename)


def is_ini_file(filename: str) -> bool:
    """Returns whether filename indicates ini file.

    Required suffix: .ini
    """
    return has_file_type(["ini"], filename)


def has_file_type(types: List[str], filename: str) -> bool:
    """Returns whether filename contains one of the provided file types."""
    for file_type in types:
        if filename.lower().endswith(f".{file_type}"):
            return True

    return False
