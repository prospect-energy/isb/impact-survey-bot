"""Contains History and HistoryEntry classes."""
from collections import deque
from typing import Optional

from survey.steps import Step
from survey.survey import Survey


class HistoryEntry:
    """Struct to store history entry."""

    def __init__(self, step: Step, survey: Survey, db_column: Optional[str]) -> None:
        self.step = step
        self.survey = survey
        self.db_column = db_column


class History:
    """Class to store and retrive history entries. Works like a stack."""

    def __init__(self) -> None:
        self.__history: deque[HistoryEntry] = deque()

    def push(self, step: Step, survey: Survey, db_column: Optional[str]) -> None:
        """Appends history entry to history."""
        self.__history.append(HistoryEntry(step, survey, db_column))

    def pop(self) -> HistoryEntry:
        """Returns latest added entry in history."""
        return self.__history.pop()

    def is_filled(self) -> bool:
        """Returns whether history contains entries."""
        return bool(self.__history)

    def clear(self) -> None:
        """Deletes all entries in history."""
        self.__history.clear()
