# Module: History

The history module contains the `History` class. A `History` object can be used to manage the history entries of a session.
The `HistoryEntry` objects contain the step associated with survey and db-column.

## When to use it?

The contents of this module are currently only used by the `Session` and `SessionHandler` class.
Storing and managing the history entries is necessary to allow for the `back` command in the surveys.
However, the `History` class can be also used as a stack-like structure with items of type `HistoryEntry`.

## How to use it?

```python
history = History()

history.push(<step>, <survey>, <db_column>)

if history.is_filled():
    entry = history.pop()

history.clear()
```

