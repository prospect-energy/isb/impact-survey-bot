"""Contains functions to generate responses on twilio request."""
from threading import Lock
from typing import Dict, Optional

from history.history import HistoryEntry
from result.status import NotSet
from session.session import Session
from structs.context import SessionContext
from structs.messages import IncommingMessage, ResponseMessage
from survey.steps import ActionStep, ResponseStep, Step, get_available_selection_options
from survey.survey import Survey
from workspace.workspace import Workspace


def create_session(workspace: Workspace, identifier: str) -> Session:
    """Initializes, starts and returns session object for participant identifier."""
    session = Session(workspace, identifier)
    session.start()

    return session


def process_new(
    session_dict: Dict[str, Session],
    sessions_mutex: Lock,
    workspace: Workspace,
    request: IncommingMessage,
) -> ResponseMessage:
    """Entry function for processing twilio request when no active session exists."""

    session = create_session(workspace, request.identifier)
    session_dict[request.identifier] = session
    return process(session_dict, sessions_mutex, session, request, True)


def process_existing(
    session_dict: Dict[str, Session],
    sessions_mutex: Lock,
    session: Session,
    request: IncommingMessage,
) -> ResponseMessage:
    """Entry function for processing twilio request when active session exists."""

    session.update_last_action_utc()
    return process(session_dict, sessions_mutex, session, request, False)


def process(
    session_dict: Dict[str, Session],
    sessions_mutex: Lock,
    session: Session,
    request: IncommingMessage,
    is_new_session: bool,
) -> ResponseMessage:
    """Wrapper function to process twilio request based on session."""

    workspace = session.workspace

    session_context = get_session_ctx(session)
    resp = process_request(
        session_context, session_dict, sessions_mutex, request, is_new_session
    )

    if resp is None:
        ended_by_command = workspace.command_handler.is_command("end", request.message)
        return end_session(
            session_dict, sessions_mutex, session_context, ended_by_command
        )

    if resp.type == "selection":
        resp.option_error = workspace.translations.get_no_selection_available(
            session.language
        )

    return resp


def process_request(
    ctx: SessionContext,
    session_dict: Dict[str, Session],
    sessions_mutex: Lock,
    request: IncommingMessage,
    is_new_session: bool,
) -> Optional[ResponseMessage]:
    """Processes the actual twilio request.
    Checks if participant input is command.
    Returns response message or None on session end.
    """

    participant_response = request.message
    session = ctx.session
    workspace = session.workspace

    current_survey: Survey = session.current_survey
    current_step: Step = session.current_step

    set_request_variables(session, request)

    # start survey and get first response
    if is_new_session:
        current_survey.start_survey(ctx)
        next_response = get_next_response(ctx, current_step.id_)
        return next_response.get_message(ctx)

    command_handler = workspace.command_handler
    last_response: ResponseStep = current_step

    # set language if language command
    if language := command_handler.is_language_command(participant_response):
        session.log_step_interaction(
            "LANGUAGE_COMMAND", {"command": participant_response}
        )
        session.set_language(ctx.postgres, language)
        return last_response.get_message(ctx)

    # check if command
    if command_handler.is_command("start", participant_response):
        session.log_step_interaction("START_COMMAND", {"command": participant_response})
        session.end()
        return process_new(session_dict, sessions_mutex, workspace, request)

    if command_handler.is_command("end", participant_response):
        session.log_step_interaction("END_COMMAND", {"command": participant_response})
        return None

    if command_handler.is_command("help", participant_response):
        session.log_step_interaction("HELP_COMMAND", {"command": participant_response})
        return process_help(ctx, last_response)

    if command_handler.is_command("back", participant_response):
        session.log_step_interaction("BACK_COMMAND", {"command": participant_response})
        return process_back(ctx)

    if command_handler.is_command("skip", participant_response):
        session.log_step_interaction("SKIP_COMMAND", {"command": participant_response})
        return process_skip(ctx, last_response)

    return process_steps(ctx, request, last_response)


def process_steps(
    ctx: SessionContext, request: IncommingMessage, last_response: ResponseStep
) -> Optional[ResponseMessage]:
    """Processes user input if it isn't a command.
    Handles modification and validation of request.
    Executes action steps to get to next response (message/selection).
    Returns None on session end.
    """

    session = ctx.session

    store_history_entry(ctx)

    request = modify_input(request, last_response)

    if not is_valid_input(request, last_response):
        if last_response.type == "message":
            session.log_step_interaction(
                "INVALID_INPUT", {"input_type": last_response.input_type}
            )
        return last_response.get_invalid_input(ctx)

    response_successor_id = last_response.successor
    if response_successor_id is None:
        return None

    set_request_variables(session, request)

    if last_response.type == "message":
        session.survey_variables[last_response.variable_id] = request.message

    response_successor_id = handle_survey_switch(ctx, response_successor_id)

    current_survey = session.current_survey
    response_successor = current_survey.steps[response_successor_id]

    if is_selection_action(response_successor):
        response_successor = get_selection_action_successor(ctx, response_successor)
        session.current_step = response_successor

    if response_successor is None:
        return None

    next_response = get_next_response(ctx, response_successor.id_)
    if next_response is None:
        return None

    session.log_step_interaction(
        "NEXT_STEP", {"step_id": next_response.id_, "step_type": next_response.type}
    )

    session.current_step = next_response
    return next_response.get_message(ctx)


def process_help(ctx: SessionContext, last_response: ResponseStep) -> ResponseMessage:
    """Processes help command.
    Returns help message for last response if available.
    Else returns no-help-available message.
    """

    session = ctx.session
    workspace = session.workspace

    help_message = last_response.get_help(ctx)
    if help_message:
        return help_message

    return workspace.translations.get_no_help_available(session.language)


def process_back(ctx: SessionContext) -> ResponseMessage:
    """Processes back command.
    Undos actions since previous response (does not apply to db queries).
    Returns previous bot response.
    """

    session = ctx.session
    workspace = session.workspace

    while session.history.is_filled():
        previous_step_entry: HistoryEntry = session.history.pop()

        step = previous_step_entry.step
        survey = previous_step_entry.survey
        db_column = previous_step_entry.db_column

        session.survey_variables[step.id_] = NotSet()
        if db_column:
            session.db_cache.pop(db_column, None)

        if step.type == "action":
            continue

        session.current_step = step
        session.current_survey = survey

        return step.get_message(ctx)

    return workspace.translations.get_no_back_available(session.language)


def process_skip(
    ctx: SessionContext, last_response: ResponseStep
) -> Optional[ResponseMessage]:
    """Processes skip command.
    Returns earliest response after skip successor
    if skip is enabled for current response step.
    """

    session = ctx.session

    skip_section = last_response.skip
    current_survey = session.current_survey

    if skip_section and skip_section.enabled:
        if skip_section.successor is None:
            return None

        session.history.push(last_response, current_survey, None)
        successor_id = handle_survey_switch(ctx, skip_section.successor)

        successor_step = current_survey.steps[successor_id]
        session.current_step = successor_step

        next_response = get_next_response(ctx, session.current_step.id_)
        if next_response is None:
            return None

        return next_response.get_message(ctx)

    return session.current_step.get_message(ctx)


def get_next_response(
    ctx: SessionContext, current_step_id: str
) -> Optional[ResponseStep]:
    """Executes action steps to get to the next response step.
    If a successor is None, function returns None.
    """

    session = ctx.session

    if current_step_id is None:
        return None

    if "$" in current_step_id:
        current_step_id = handle_survey_switch(ctx, current_step_id)

    current_survey = session.current_survey
    current_step = current_survey.steps[current_step_id]

    if not current_step.condition_fulfilled(ctx):
        successor_id = current_step.condition.successor
        session.log_step_interaction("CONDITION_SKIP", {"successor": successor_id})
        return get_next_response(ctx, successor_id)

    if current_step.type == "message":
        return current_step

    if current_step.type == "selection":
        options = get_available_selection_options(
            current_step.options, ctx.session.groups
        )
        if len(options) == 1:
            successor_id = options[0].successor
            return get_next_response(ctx, successor_id)

        return current_step

    action_output = current_step.execute(ctx)
    successor_id = action_output.successor_id
    if successor_id is None:
        return None

    session.survey_variables[current_step_id] = action_output.return_value
    session.history.push(current_step, session.current_survey, None)

    return get_next_response(ctx, successor_id)


def handle_survey_switch(ctx: SessionContext, step_id: str) -> str:
    """Convenience function to check for and handle transition between surveys."""

    session = ctx.session

    if "$" in step_id:
        survey_name = step_id.replace("$", "")
        session.current_survey.finish_survey(ctx, False)
        next_survey = session.workspace.surveys[survey_name]
        session.set_current_survey(next_survey)
        next_survey.start_survey(ctx)
        return session.current_step.id_

    return step_id


def end_session(
    sessions: Dict[str, Session],
    mutex: Lock,
    ctx: SessionContext,
    ended_by_command: bool,
) -> ResponseMessage:
    """Ends participant session and returns session-ended response message."""

    session = ctx.session
    workspace = session.workspace

    mutex.acquire()
    sessions[session.identifier] = None
    mutex.release()

    resp = session.current_survey.finish_survey(ctx, ended_by_command)
    session.end()
    if resp:
        return resp

    return workspace.translations.get_session_ended(session.language)


def set_request_variables(session: Session, request: IncommingMessage) -> None:
    """Sets/Overwrites request variables in session."""

    session.variables["request.input"] = request.message
    session.variables["request.media"] = request.media
    session.variables["request.lat"] = request.latitude
    session.variables["request.lng"] = request.longitude
    session.variables["request.identifier"] = request.identifier


def modify_input(
    request: IncommingMessage, last_response: ResponseStep
) -> IncommingMessage:
    """Modifies participant input using defined modifies of last response step."""

    if last_response.type == "message":
        request = last_response.modify_input(request)

    return request


def is_valid_input(request: IncommingMessage, last_response: ResponseStep) -> bool:
    """Validates input-type of participant input."""

    if last_response.type == "message":
        return last_response.validate_input_type(request)

    return True


def is_selection_action(step: Step) -> bool:
    """Checks if current step is action step and has selection-action function."""

    return isinstance(step, ActionStep) and step.is_selection_action


def get_selection_action_successor(
    ctx: SessionContext, selection_action: ActionStep
) -> Step:
    """Processes selection action function to get successor of selection step."""

    successor_id = process_selection_action(ctx, selection_action)

    return ctx.session.current_survey.steps[successor_id]


def process_selection_action(ctx: SessionContext, action_step: ActionStep) -> str:
    """Executes selection action function using participant input (e.g. selected number)."""

    session = ctx.session

    action_output = action_step.execute(ctx)
    session.survey_variables[session.current_step.id_] = action_output.return_value
    return handle_survey_switch(ctx, action_output.successor_id)


def store_history_entry(ctx: SessionContext) -> None:
    """Stores current step, output and db-column in session history."""

    session = ctx.session

    current_step = session.current_step
    db_column = (
        current_step.db_column
        if current_step.type in ["message", "selection"]
        else None
    )

    session.history.push(session.current_step, session.current_survey, db_column)


def get_session_ctx(session: Session) -> SessionContext:
    """Convenience function to get session-context of session."""

    return session.workspace.get_session_context(session)
