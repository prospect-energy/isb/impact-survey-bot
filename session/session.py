"Contains session class."
import json
import uuid
from datetime import datetime
from typing import Any, Dict, List, Optional

from database.components import ArrayAggregate, Column, Table, WhereClause
from database.database import Postgres
from database.queries import get_participant_data
from database.serializer import default_json_serializer
from history.history import History
from survey.steps import Step
from survey.survey import Survey
from workspace.workspace import Workspace


class Session:
    "A class for storing and handling participant session data."

    def __init__(self, workspace: Workspace, identifier: str) -> None:
        self.__is_registered: bool = False
        self.__participant_name: str = ""

        self.workspace: Workspace = workspace
        self.identifier: str = identifier
        self.uuid: str = str(uuid.uuid4())

        self.last_action_utc: datetime = datetime.now()
        self.survey_started_timestamp: datetime = datetime.now()

        self.participant_id: str = ""
        self.language: str = self.workspace.ctx.default_language
        self.groups: List[str] = []

        self.history: History = History()
        self.db_cache: Dict[str, Any] = {}
        self.custom_fields: Dict[str, Any] = {}
        self.variables: Dict[str, Any] = {}
        self.survey_variables: Dict[str, Any] = {}
        self.interaction_log: List[dict] = []

        self.current_step: Optional[Step] = None
        self.current_survey: Optional[Survey] = None

    def start(self) -> None:
        """(Re-)Starts session by resetting current step and survey
        as well as getting up-to-date participant data from the database.add()
        """
        self.log_interaction("SESSION_START", {})

        self.variables.clear()
        self.survey_variables.clear()
        self.history.clear()

        self.update_last_action_utc()
        postgres = self.workspace.ctx.postgres

        participants_table = Table(postgres.shared_schema, "participants")
        groups_table = Table(postgres.shared_schema, "groups")
        row = get_participant_data(
            postgres,
            self.workspace.workspace_id,
            self.identifier,
            [
                Column(participants_table, "participant_id"),
                Column(participants_table, "language"),
                Column(participants_table, "firstname"),
                Column(participants_table, "lastname"),
                Column(participants_table, "is_registered"),
                ArrayAggregate(Column(groups_table, "name"), "groups"),
            ],
        )

        self.participant_id = row[0]
        self.language = row[1]

        self.__participant_name = f"{row[2]} {row[3]}"
        self.__is_registered = row[4]
        self.groups = row[5]

        if not self.__is_registered:
            self.set_current_survey(self.workspace.registration_survey)
        else:
            self.set_current_survey(self.workspace.selection_survey)

        self.variables = {
            "session.uuid": self.uuid,
            "session.identifier": self.identifier,
            "session.participant_id": self.participant_id,
            "session.name": self.__participant_name,
            "session.db_cache": self.db_cache,
            "session.groups": self.groups,
        }

    def end(self) -> None:
        self.log_interaction("SESSION_END", {})
        self.__log_session()

    def update_last_action_utc(self) -> None:
        """Convenience function for setting last-action timestamp to current datetime."""
        self.last_action_utc = datetime.now()

    def set_current_survey(self, survey: Survey) -> None:
        """Convenience function for setting current survey
        and automatically setting starting step.
        """
        self.survey_variables = {}
        self.current_survey = survey
        self.current_step = survey.starting_step

    def set_language(self, db_: Postgres, language: str) -> None:
        """Sets the language of the current session and updates participant entry in database."""
        self.language = language
        shared_schema = db_.shared_schema

        if self.participant_id:
            table = Table(shared_schema, "participants")
            db_.update(
                table,
                [WhereClause(Column(table, "participant_id"), self.participant_id)],
                {"language": language},
            )

    def get_all_variables(self) -> Dict[str, Any]:
        """Convenience funtion for retrieving all session variables as single dictionary.

        Contains:
            - request-variables ('request.')
            - step-variables ('step.')
            - session-variables ('session.')
            - user-defined variables ('var.')
        """
        return {**self.variables, **self.survey_variables}

    def log_survey_interaction(self, type_: str):
        self.log_interaction(type_, {"survey": self.current_survey.name})

    def log_step_interaction(self, type_: str, data: Dict[str, Any] = {}):
        self.log_interaction(
            type_,
            {
                "survey": self.current_survey.name,
                "step_id": self.current_step.id_,
                "step_type": self.current_step.type,
                **data,
            },
        )

    def log_interaction(self, type_: str, data: Dict[str, Any]):
        self.interaction_log.append(
            {"log_type": type_, "timespamp": datetime.now(), **data}
        )

    def __log_session(self) -> None:
        """Logs session data to 'sessions' database table."""
        postgres = self.workspace.ctx.postgres

        logs = {"version": "1", "logs": self.interaction_log}

        postgres.insert(
            table=Table(postgres.system_schema, "sessions"),
            data=[
                {
                    "session_uuid": self.uuid,
                    "logs": json.dumps(logs, default=default_json_serializer),
                    "created_at": datetime.now(),
                    "created_by": "isb",
                }
            ],
            error_on_conflict=False,
        )
