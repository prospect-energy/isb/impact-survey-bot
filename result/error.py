"""Contains custom exception classes."""


import json
from typing import Any, Dict


class BotException(Exception):
    """Exception base class for all custom errors with optional detail dictionary."""

    def __init__(self, message: str, detail: Dict[str, Any] = {}) -> None:
        super().__init__(message)

        self.message = message
        self.detail = detail

    def get_log_message(self):
        return f"""{self.message}
                    Detail: {json.dumps(self.detail)}"""


class ComponentException(BotException):
    """Exception base class for errors connected to survey components and params."""

    exception_type = "component"


class EmptyValueException(ComponentException):
    """Exception class for missing field or value."""

    exception_type = "empty_value"


class InvalidTypeException(ComponentException):
    """Exception class for invalid value type."""

    exception_type = "invalid_type"


class MandatoryException(ComponentException):
    """Exception class for missing mandatory field."""

    exception_type = "mandatory_field"


class WorkspaceException(BotException):
    """Exception base class for errors regarding workspace initialization."""

    exception_type = "workspace"


class SurveyException(WorkspaceException):
    """Exception class for error during survey parsing."""

    exception_type = "survey"


class ConfigException(WorkspaceException):
    """Exception class for error during configuration parsing."""

    exception_type = "config"


class ValidationException(WorkspaceException):
    """Exception class for errors during survey validation."""

    exception_type = "validation"


class MigrationException(WorkspaceException):
    """Exception class for errors during survey validation."""

    exception_type = "migration"


class TranslationException(WorkspaceException):
    """Exception class for errors during translation parsing."""

    exception_type = "translation"


class DatabaseException(Exception):
    """Exception class for errors during database queries."""


class FileException(Exception):
    """Exception class for errors during file download, deletion or upload."""
