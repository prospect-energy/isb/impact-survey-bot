"""Contains wrapper functions and utilities for migrating survey tables."""
from __future__ import annotations

from typing import TYPE_CHECKING, Dict

from database.queries import migrate_table

if TYPE_CHECKING:
    from survey.survey import Survey

from typing import List

from database.components import Table
from database.database import Postgres

TIME_WITHOUT_TIMEZONE = "time without time zone"
TEXT = "text"
NUMERIC = "numeric"

TYPE_TRANSLATIONS = {
    "text-100": "character varying(100)",
    "text-long": TEXT,
    "integer": "integer",
    "decimal": NUMERIC,
    "number": NUMERIC,
    "location": NUMERIC,
    "picture": TEXT,
    "date": "date",
    "time": TIME_WITHOUT_TIMEZONE,
    "time-12": TIME_WITHOUT_TIMEZONE,
    "time-24": TIME_WITHOUT_TIMEZONE,
    "url": TEXT,
    "phonenumber": "character varying(50)",
    "boolean": "boolean",
    "timestamp": "timestamp without time zone",
}


def translate_column_types(
    column_dict: Dict[str, str], primary_key: str
) -> Dict[str, str]:
    """Translates datatype names to sql specific datatypes and returns adapted dict."""
    sql_column_dict = {}

    if column_dict.get(primary_key):
        del column_dict[primary_key]

    for column, input_type in column_dict.items():
        sql_column_dict[column] = TYPE_TRANSLATIONS.get(input_type, TEXT)

    return sql_column_dict


def get_length_insensitive_column_dict(
    sql_column_dict: Dict[str, str]
) -> Dict[str, str]:
    """Returns sql_column_dict with length-insensitive datatypes."""
    length_insensitive_dict = {}

    for column, column_type in sql_column_dict.items():
        if "character varying" in column_type:
            length_insensitive_dict[column] = "character varying"
        else:
            length_insensitive_dict[column] = column_type

    return length_insensitive_dict


def migrate_survey_tables(
    postgres: Postgres, workspace_schema: str, surveys: List[Survey]
) -> None:
    """Mirgrates workspace schema based on provided survey objects."""
    for survey in surveys.values():
        if not survey.db_enabled:
            continue

        table_name = survey.config.table
        primary_key = survey.config.id_column

        sql_column_dict = translate_column_types(survey.db_columns, primary_key)
        length_insensitive_column_dict = get_length_insensitive_column_dict(
            sql_column_dict
        )

        postgres.create_table(
            Table(workspace_schema, table_name), sql_column_dict, primary_key
        )

        migrate_table(
            postgres,
            Table(workspace_schema, table_name),
            sql_column_dict,
            length_insensitive_column_dict,
        )
