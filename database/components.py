"""Contains query component classes."""
from __future__ import annotations

from enum import Enum
from typing import TYPE_CHECKING, Any, List, Optional, Union

if TYPE_CHECKING:
    from database.models import ComponentQuery, PlainQuery

from psycopg2 import sql


class FetchPolicy(Enum):
    FETCH_ONE = "fetch_one"
    FETCH_ALL = "fetch_all"


class Table:
    """Class to wrap postgres table name."""

    def __init__(self, schema: str, name: str) -> None:
        self.schema = schema
        self.name = name

    def get_db_identifier(self) -> sql.SQL:
        """Formats and returns sql string from class attributes."""
        return sql.SQL(".").join(
            [sql.Identifier(self.schema), sql.Identifier(self.name)]
        )

    def get_full_name(self) -> str:
        """Return full name for table."""
        return ".".join([self.schema, self.name])


class Column:
    """Class to wrap postgres column name."""

    def __init__(
        self, table: Table, column: str, type_cast: Optional[str] = None
    ) -> None:
        self.__schema = table.schema
        self.__table = table.name
        self.__column = column
        self.__type_cast = type_cast

    def get_db_identifier(self) -> sql.SQL:
        """Formats and returns sql string from class attributes."""
        sql_str = sql.SQL(".").join(
            [
                sql.Identifier(self.__schema),
                sql.Identifier(self.__table),
                sql.Identifier(self.__column),
            ]
        )

        if self.__type_cast:
            return sql.SQL("").join(
                [
                    sql_str,
                    sql.SQL("::{type_name}").format(
                        type_name=sql.SQL(self.__type_cast)
                    ),
                ]
            )

        return sql_str

    def get_full_name(self) -> str:
        """Return full name for column."""
        return ".".join([self.__schema, self.__table, self.__column])


class ArrayColumn:
    """Class to wrap postgres array aggregate clause."""

    def __init__(self, columns: List[Column]) -> None:
        self.__columns = columns

    def get_db_identifier(self) -> sql.SQL:
        """Formats and returns sql clause from class attributes."""
        return sql.SQL("ARRAY[{columns}]").format(
            columns=sql.SQL(",").join(c.get_db_identifier() for c in self.__columns)
        )


class ArrayAggregate:
    """Class to wrap postgres array aggregate clause."""

    def __init__(self, column: Column, name: str) -> None:
        self.__column = column
        self.__name = name

    def get_db_identifier(self) -> sql.SQL:
        """Formats and returns sql clause from class attributes."""
        return sql.SQL("ARRAY_AGG({column}) AS {name}").format(
            column=self.__column.get_db_identifier(),
            name=sql.Identifier(self.__name),
        )


class QueryColumn:
    """Class to wrap a nested postgres query."""

    def __init__(
        self,
        query: Union[ComponentQuery, PlainQuery],
        name_as: str,
        as_array: Optional[bool] = False,
    ) -> None:
        self.query = query
        self.name_as = name_as
        self.as_array = as_array

    def get_db_identifier(self) -> sql.SQL:
        """Formats and returns sql clause from class attributes."""
        if self.as_array:
            return sql.SQL("array({query}) as {column_name}").format(
                query=self.query.get_sql_str(), column_name=sql.Identifier(self.name_as)
            )

        return sql.SQL("({query}) as {column_name}").format(
            query=self.query.get_sql_str(), column_name=sql.Identifier(self.name_as)
        )


class JoinTable:
    """Class to wrap postgres join clause."""

    def __init__(self, table: Table, left_column: Column, right_column: Column) -> None:
        self.__table = table
        self.__left_column = left_column
        self.__right_column = right_column

    def get_sql_str(self) -> sql.SQL:
        """Formats and returns sql clause from class attributes."""
        return sql.SQL("JOIN {table} ON {left_column} = {right_column}").format(
            table=self.__table.get_db_identifier(),
            left_column=self.__left_column.get_db_identifier(),
            right_column=self.__right_column.get_db_identifier(),
        )


class WhereClause:
    """Class to wrap postgres where clause."""

    def __init__(
        self, column: Column, value: Union[Any, Column], operator: str = "="
    ) -> None:
        self.__column = column
        self.__value = value
        self.__operator = operator

    def get_sql_str(self) -> sql.SQL:
        """Formats and returns sql clause from class attributes."""
        if self.__operator not in ["=", "<", ">", "<=", ">=", "<>", "!=", "IS"]:
            self.__operator = "="

        if isinstance(self.__value, list):
            return sql.SQL("{column} in ({value})").format(
                column=self.__column.get_db_identifier(),
                value=sql.SQL(",").join([sql.Literal(val) for val in self.__value]),
            )

        return sql.SQL("{column} {operator} {value}").format(
            column=self.__column.get_db_identifier(),
            value=(
                self.__value.get_db_identifier()
                if isinstance(self.__value, Column)
                else sql.Literal(self.__value)
            ),
            operator=sql.SQL(self.__operator),
        )


class GroupBy:
    """Class to wrap postgres group by clause."""

    def __init__(self, columns: List[Union[Column, ArrayAggregate]]) -> None:
        self.__columns = columns

    def get_sql_str(self) -> sql.SQL:
        """Formats and returns sql clause from class attributes."""
        return sql.SQL("GROUP BY {columns}").format(
            columns=sql.SQL(",").join([c.get_db_identifier() for c in self.__columns])
        )


class OrderBy:
    """Class to wrap postgres oder by clause."""

    def __init__(self, column: Column, direction: str = "ASC") -> None:
        self.__column = column
        self.__direction = direction

    def get_sql_str(self) -> sql.SQL:
        """Formats and returns sql clause from class attributes."""
        query = sql.SQL("ORDER BY {column}").format(
            column=self.__column.get_db_identifier()
        )

        direction_sql = None
        if str(self.__direction).lower() in ["asc", "desc"]:
            direction_sql = sql.SQL(self.__direction)
        else:
            direction_sql = sql.SQL("ASC")

        return sql.SQL(" ").join([query, direction_sql])
