"""Contains Postgres class."""
from typing import Any, Callable, Optional, Union

from psycopg2 import DatabaseError, connect, sql

from database.components import FetchPolicy, Table
from database.models import ComponentQuery, PlainQuery
from database.query_interface import QueryInterface
from database.transaction import TransactionContext
from logger.logger import Logger
from result.error import DatabaseException


class Postgres(QueryInterface):
    """Class to manage postgres connection and execute postgres queries."""

    def __init__(self, config: dict, logger: Logger) -> None:
        super().__init__()

        self.__config = config
        self.__logger = logger

        self.__connection = None

        self.__connection_params = {
            "host": self.__config["host"],
            "port": self.__config["port"],
            "database": self.__config["database"],
            "user": self.__config["user"],
            "password": self.__config["password"],
        }

        self.system_schema = self.__config["system_schema"]
        self.shared_schema = self.__config["shared_schema"]

    def connect(self) -> None:
        """Connects to postgres server using configurated credentials."""
        self.close()
        try:
            self.__connection = connect(**self.__connection_params)
            self.__logger.debug(f"Using database: {self.__connection_params['host']}")
        except (Exception, DatabaseError) as error:
            self.__logger.error(f"Could not connect to database - error: {error}")
            raise DatabaseException from error

    def connected(self) -> bool:
        """Checks if connection to postgres exists and isn't closed."""
        return self.__connection and self.__connection.closed == 0

    def close(self) -> None:
        """Closes postgres connection if connected."""
        if self.connected():
            try:
                self.__connection.close()
            except Exception:
                pass

        self.__connection = None

    def reconnect(function: Callable) -> Any:
        """Decorator function to connect and close connection to postgres server automatically."""

        def function_wrapper(postgres, *args, **kwargs):
            if not postgres.connected():
                postgres.connect()

            try:
                return function(postgres, *args, **kwargs)
            except DatabaseError:
                postgres.close()
                raise

        return function_wrapper

    @reconnect
    def execute(
        self,
        query: Union[ComponentQuery, PlainQuery],
        fetch: Optional[FetchPolicy] = None,
    ) -> Optional[Any]:
        """Executes a single sql query as transaction."""
        with self.__connection, self.__connection.cursor() as cursor:
            return self._QueryInterface__execute(
                self.__logger, self.__connection, cursor, query, fetch
            )

    @reconnect
    def transaction(self, func: Callable, *args, **kwargs) -> Optional[Any]:
        """Executes a function (containing sql queries) as a single transaction."""
        with self.__connection, self.__connection.cursor() as cursor:
            return func(
                TransactionContext(self, self.__logger, self.__connection, cursor),
                *args,
                **kwargs,
            )

    @reconnect
    def fix_column_sequence(self, table: Table, column_name: str) -> Any:
        """Resets value of sequence for specific table and column on highest value."""
        with self.__connection, self.__connection.cursor() as cursor:
            sequence_name = f"{table.get_full_name()}_{column_name}_seq"

            query = (
                sql.SQL(
                    "SELECT SETVAL({sequence_name}, COALESCE(MAX({column_name}), 1) ) FROM {table};"
                )
                .format(
                    sequence_name=sql.Literal(sequence_name),
                    column_name=sql.Identifier(column_name),
                    table=table.get_db_identifier(),
                )
                .as_string(self.__connection)
            )

            self.__logger.debug(f"Executed query: '{query}'")
            cursor.execute(query)
