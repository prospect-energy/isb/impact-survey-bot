# Module: Storage

The storage module contains the `S3` class which wraps an AWS S3 client to store files in a S3 bucket.
In addition the module contains support functions for the different operations on the client.

## When to use it?

You can use this module if you want to use or test the `S3` class.
Currently, `ImpactSurveyBot` only defines one `S3` object which is used across all workspaces.
If other storage clients are added, these should be located in this module as well.

## How to use it?

```python
s3_config = {
    "access_key": <"aws access key">
    "secret_key": <"aws secret key">
    "bucket": <"name of s3 bucket">
    "local_directory": <"local directory for temporary storage">
}

storage = S3(
    config = s3_config,
    logger = <Logger obj>
)
```

__Make sure to use environmental variables for access_key and secret_key!__
