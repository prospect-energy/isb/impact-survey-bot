"""Contains S3 class."""
from typing import Any, Dict
from uuid import uuid4

import boto3
import requests
from boto3.exceptions import S3UploadFailedError
from botocore.exceptions import ClientError

from result.error import FileException
from storage.files import get_extension_from_content_type


class S3:
    """Class to manage uploads to AWS S3 and MinIO."""

    def __init__(self, config: Dict[str, Any]) -> None:
        access_key = config["access_key"]
        secret_key = config["secret_key"]
        endpoint = config["endpoint"]
        bucket = config["bucket"]
        local_directory = config["localpath"]

        if not bucket:
            raise Exception("Missing name for bucket")

        if not local_directory:
            raise Exception("Missing value for localpath")

        self.__bucket = bucket

        self.__client = boto3.client(
            "s3",
            endpoint_url=endpoint,
            aws_access_key_id=access_key,
            aws_secret_access_key=secret_key,
        )

    def upload_file_from_url(
        self, url: str, download_headers: Dict[str, Any], workspace_asset_key: str
    ) -> str:
        """Uploads file from url to AWS S3 or MinIO.
        Automatically downloads and deletes file.
        """
        filename = str(uuid4())

        resp = requests.get(url, headers=download_headers, allow_redirects=True)
        if resp.status_code != 200:
            raise FileException("Url for download returned a non-200 statuscode")

        content_type = resp.headers.get("content-type")

        file_extension = get_extension_from_content_type(content_type)
        if not file_extension:
            file_extension = url.split(".")[-1]

        filename_with_extension = ".".join([filename, file_extension])
        s3_path = f"{workspace_asset_key}/{filename_with_extension}"

        try:
            self.__client.put_object(
                Bucket=self.__bucket, Key=s3_path, Body=resp.content
            )
        except (ClientError, S3UploadFailedError) as error:
            raise FileException(f"Cannot upload to bucket - error: {error}") from error

        return filename_with_extension
