"""Contains all routes of application."""
import hashlib
import hmac
import logging
import os

from fastapi import Depends, FastAPI, HTTPException, Query, Request, Response
from fastapi.responses import JSONResponse
from twilio.request_validator import RequestValidator

from bot.bot import ImpactSurveyBot
from request.telegram import TelegramRequest
from request.twilio import TwilioRequest
from request.update import UpdateRequest
from request.whatsapp import WhatsAppRequest
from result.error import BotException
from structs.messages import IncommingMessage, Media, MediaOrigin
from util import get_bot_config

TWILIO_ENDPOINTS_ENABLED = os.environ.get("TWILIO_ENDPOINTS_ENABLED", False)
TWILIO_AUTH_ENABLED = os.environ.get("TWILIO_AUTH_ENABLED", False)
TWILIO_AUTH_TOKEN = os.environ.get("TWILIO_AUTH_TOKEN", None)
WAB_ENDPOINTS_ENABLED = os.environ.get("WAB_ENDPOINTS_ENABLED", False)
WAB_VERIFY_TOKEN = os.environ.get("WAB_VERIFY_TOKEN", None)
WAV_AUTH_ENABLED = os.environ.get("WAB_AUTH_ENABLED", False)
WAB_APP_SECRET = os.environ.get("WAB_APP_SECRET", None)
TELEGRAM_ENDPOINTS_ENABLED = os.environ.get("TELEGRAM_ENDPOINTS_ENABLED", False)
TELEGRAM_AUTH_ENABLED = os.environ.get("TELEGRAM_AUTH_ENABLED", False)
TELEGRAM_SECRET_TOKEN = os.environ.get("TELEGRAM_SECRET_TOKEN", None)
UPDATE_API_TOKEN = os.environ.get("API_TOKEN", None)


app = FastAPI()
bot = ImpactSurveyBot(get_bot_config())


class HealthCheckFilter(logging.Filter):
    def filter(self, record: logging.LogRecord) -> bool:
        return record.args and len(record.args) >= 3 and record.args[2] != "/"


logging.getLogger("uvicorn.access").addFilter(HealthCheckFilter())


@app.middleware("http")
async def add_security_headers(request: Request, call_next):
    """Middleware to add recommended security-headers."""
    response = await call_next(request)

    response.headers["Cache-Control"] = "no-store"
    response.headers["Strict-Transport-Security"] = "max-age=63072000; preload"
    response.headers["X-Content-Type-Options"] = "nosniff"
    response.headers["X-Frame-Options"] = "DENY"
    response.headers[
        "Content-Security-Policy"
    ] = "default-src 'self'; script-src 'self' https://cdn.jsdelivr.net/npm/swagger-ui-dist@3/swagger-ui-bundle.js 'sha256-R2r7jpC1j6BEeer9P/YDRn6ufsaSnnARhKTdfrSKStk='; style-src 'self' https://cdn.jsdelivr.net/npm/swagger-ui-dist@3/swagger-ui.css; frame-ancestors 'none'"
    response.headers["Feature-Policy"] = "'none'"
    response.headers["Referrer-Policy"] = "no-referrer"

    return response


@app.get("/")
def root():
    """Root route to check if server is running."""
    return {"message": "Hello World"}


if TWILIO_ENDPOINTS_ENABLED:

    @app.post("/twilio")
    async def twilio(
        raw: Request, request: TwilioRequest = Depends(TwilioRequest.as_form)
    ):
        """Webhook route for communicating with twilio."""

        if TWILIO_AUTH_ENABLED:
            validator = RequestValidator(TWILIO_AUTH_TOKEN)
            form = await raw.form()
            if not validator.validate(
                str(raw.url), form, raw.headers.get("X-Twilio-Signature", "")
            ):
                raise HTTPException(status_code=400, detail="Error in signature")

        incomming_message = IncommingMessage(
            identifier=request.phone_number,
            message=request.message,
            latitude=request.latitude,
            longitude=request.longitude,
            media=Media(
                origin=MediaOrigin.TWILIO,
                url=request.media,
                content_type=request.media_content_type,
            ),
        )
        response = bot.process(incomming_message).twilio()
        return Response(content=response, media_type="application/xml")


if WAB_ENDPOINTS_ENABLED:

    @app.get("/whatsapp")
    async def whatsapp_validation(
        mode: str = Query(..., alias="hub.mode"),
        challenge: str = Query(..., alias="hub.challenge"),
        verify_token: str = Query(..., alias="hub.verify_token"),
    ):
        """Webhook route for verifing the webhook for the WhatsApp Business API."""
        if mode == "subscribe" and verify_token == WAB_VERIFY_TOKEN:
            return int(challenge)

        raise HTTPException(status_code=400, detail="Invalid request")


if WAB_ENDPOINTS_ENABLED:

    @app.post("/whatsapp")
    async def whatsapp_messages(raw: Request, request: WhatsAppRequest):
        """Webhook route for receiving message requests from WhatsApp Business API."""

        if not request.is_message_request():
            return

        if WAV_AUTH_ENABLED:
            payload = await raw.body()
            signature = raw.headers.get("x-hub-signature-256", "")
            control_signature = hmac.new(
                WAB_APP_SECRET.encode("ASCII"), payload, hashlib.sha256
            ).hexdigest()

            if signature != f"sha256={control_signature}":
                raise HTTPException(status_code=400, detail="Error in signature")

        phone_number = request.get_phone_number()
        latitude, longitude = request.get_coordinates()

        incomming_message = IncommingMessage(
            identifier=phone_number,
            message=request.get_message(),
            latitude=latitude,
            longitude=longitude,
            media=request.get_media(),
        )

        response = bot.process(incomming_message)
        bot.send_whatsapp(request.get_phone_number(), response)
        return


if TELEGRAM_ENDPOINTS_ENABLED:

    @app.post("/telegram")
    async def telegram_messages(raw: Request, request: TelegramRequest):
        """Webhook route for receiving message requests from Telegram."""

        if not request.is_message_request():
            return

        if TELEGRAM_AUTH_ENABLED:
            submitted_token = raw.headers.get("x-telegram-bot-api-secret-token", "")

            if TELEGRAM_SECRET_TOKEN != submitted_token:
                raise HTTPException(status_code=400, detail="Error in signature")

        identifier = request.get_identifier()
        latitude, longitude = request.get_coordinates()

        incomming_message = IncommingMessage(
            identifier=identifier,
            message=request.get_message(),
            latitude=latitude,
            longitude=longitude,
            media=request.get_media(),
        )

        response = bot.process(incomming_message)
        bot.send_telegram(identifier, response)
        return


@app.post("/update", status_code=200)
def update(raw: Request, request: UpdateRequest):
    """Route to notify bot instance of workspace update."""
    submitted_token = raw.headers.get("ISB-API-Token", "")

    if UPDATE_API_TOKEN != submitted_token:
        raise HTTPException(status_code=400, detail="Access Token is invalid")

    try:
        bot.update(request)
    except BotException as error:
        bot.logger.error(
            f"""Workspace: {request.workspace_id}
                    {error.get_log_message()}"""
        )
        return JSONResponse(
            status_code=400,
            content={
                "status": "error",
                "message": str(error),
                "exception_type": None,
                "detail": error.detail,
            },
        )

    return JSONResponse(status_code=200, content={"status": "success"})
