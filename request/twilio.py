"""Contains request models for Twilio."""
from typing import Optional

from fastapi import Form
from pydantic import BaseModel


class TwilioRequest(BaseModel):
    """Class for conversion and validation of twilio request."""

    phone_number: str
    message: Optional[str]
    latitude: Optional[str]
    longitude: Optional[str]
    media: Optional[str]
    media_content_type: Optional[str]

    @classmethod
    def as_form(
        cls,
        From: str = Form(...),
        Body: Optional[str] = Form(""),
        Latitude: Optional[str] = Form(""),
        Longitude: Optional[str] = Form(""),
        MediaUrl0: Optional[str] = Form(""),
        MediaContentType0: Optional[str] = Form(""),
    ) -> "TwilioRequest":
        """Returns class object from xml-form-encoded request data."""

        return cls(
            phone_number=str(From)
            .split(":")[1]
            .translate({ord(i): None for i in " ()-"}),
            message=Body,
            latitude=Latitude,
            longitude=Longitude,
            media=MediaUrl0,
            media_content_type=MediaContentType0,
        )
