"""Contains structs for selection step attributes."""
from typing import Any, List, Optional

from structs.messages import TranslatedMessage


class SelectionOption:
    """Struct for storing data of a selection option."""

    def __init__(
        self,
        text: TranslatedMessage,
        output: Any,
        output_is_variable: bool,
        successor: str,
    ) -> None:
        self.text = text
        self.output = output
        self.output_is_variable = output_is_variable
        self.successor = successor


class SelectedItemsList:
    """Struct for storing selected options for selection step."""

    def __init__(self, items: List[SelectionOption]) -> None:
        self.items = items
        self.output = self.__join_output()
        self.successor = self.__get_successor()

    def __join_output(self) -> str:
        """Joins selected items to comma-seperated string."""
        if len(self.items) == 1:
            return self.items[0].output

        return ",".join([str(option.output) for option in self.items])

    def __get_successor(self) -> Optional[str]:
        """Returns successor of of first item in selected items list.
        Returns None if selected items list is empty.
        """
        if len(self.items) > 0:
            return self.items[0].successor

        return None
