"Contains Translations class."
import copy
from typing import Any, Dict

import parsing.util as p_util
from result.error import TranslationException
from structs.context import WorkspaceContext
from structs.messages import ResponseMessage, TranslatedMessage


class Translations:
    """Class to parse and manage the translations for a workspace."""

    def __init__(self, workspace_ctx: WorkspaceContext, config: Dict[str, Any]) -> None:
        self.ctx = workspace_ctx

        self.__key_message_dict: Dict[str, TranslatedMessage] = {}

        self.__raw_translations: dict = config["translations"]
        self.__enabled_languages: Dict[str, Dict[str, str]] = config[
            "enabled_languages"
        ]
        self.__mandatory_translations: Dict[str, str] = config["mandatory_translations"]

        self.__session_ended_key = self.__mandatory_translations["session-ended"]
        self.__no_help_key = self.__mandatory_translations["no-help-available"]
        self.__no_back_key = self.__mandatory_translations["back-not-available"]
        self.__survey_selection_key = self.__mandatory_translations["survey-selection"]
        self.__no_selection_key = self.__mandatory_translations[
            "selection-not-available"
        ]

        self.invalid_input_keys: Dict[str, str] = {
            "text-long": None,
            "text-100": self.__mandatory_translations["invalid-input-text-100"],
            "integer": self.__mandatory_translations["invalid-input-integer"],
            "decimal": self.__mandatory_translations["invalid-input-decimal"],
            "number": self.__mandatory_translations["invalid-input-number"],
            "location": self.__mandatory_translations["invalid-input-location"],
            "picture": self.__mandatory_translations["invalid-input-picture"],
            "audio": self.__mandatory_translations["invalid-input-audio"],
            "video": self.__mandatory_translations["invalid-input-video"],
            "document": self.__mandatory_translations["invalid-input-document"],
            "date": self.__mandatory_translations["invalid-input-date"],
            "time": self.__mandatory_translations["invalid-input-time"],
            "time-12": self.__mandatory_translations["invalid-input-time12"],
            "time-24": self.__mandatory_translations["invalid-input-time24"],
            "url": self.__mandatory_translations["invalid-input-url"],
            "phonenumber": self.__mandatory_translations["invalid-input-phone-number"],
        }

        self.__parse()

        self.__message_key_exists(self.__session_ended_key)
        self.__message_key_exists(self.__no_help_key)
        self.__message_key_exists(self.__no_back_key)
        self.__message_key_exists(self.__survey_selection_key)
        self.__message_key_exists(self.__no_selection_key)

        for key in self.invalid_input_keys.values():
            if key:
                self.__message_key_exists(key)

    def get_translated_message(self, key: str) -> TranslatedMessage:
        """Returns translated message for message key.
        If key is not associated with a translation, a translated message
        with key as translation in default language is returned.
        """
        translated_message = self.__key_message_dict.get(key)
        if not translated_message:
            return TranslatedMessage({self.ctx.default_language: key})

        return self.__key_message_dict[key]

    def get_session_ended(self, language: str) -> ResponseMessage:
        """Returns response message in specified language to indicate that session ended."""
        translated_message = self.__key_message_dict[self.__session_ended_key]
        return translated_message.to_response(self.ctx, language)

    def get_no_help_available(self, language: str) -> ResponseMessage:
        """Returns response message in specified language
        to indicate that no help message is available.
        """
        translated_message = self.__key_message_dict[self.__no_help_key]
        return translated_message.to_response(self.ctx, language)

    def get_no_back_available(self, language: str) -> ResponseMessage:
        """Returns response message in specified language
        to indicate that returning to previous step is not possible.
        """
        translated_message = self.__key_message_dict[self.__no_back_key]
        return translated_message.to_response(self.ctx, language)

    def get_invalid_input_tm(self, input_type: str) -> ResponseMessage:
        """Returns response message in specified language
        to indicate that input doesn't match input_type.
        """
        message_key = self.invalid_input_keys[input_type]
        return self.__key_message_dict.get(message_key)

    def get_survey_selection_tm(self) -> TranslatedMessage:
        """Returns translated message object for user-defined survey selection header message."""
        return self.__key_message_dict[self.__survey_selection_key]

    def get_no_selection_available(self, language: str) -> str:
        """Returns user-defined string in specified language to indicate option error."""
        translated_message = self.__key_message_dict[self.__no_selection_key]
        return translated_message.get_message(self.ctx, language)

    def __parse(self) -> None:
        """Parses the raw translations for the workspace and stores the results."""
        for key, message_dict in self.__raw_translations.items():
            cleaned_key = key.strip()

            if not isinstance(message_dict, dict):
                raise TranslationException(
                    f"Invalid syntax. Key {key} needs dictionary as value"
                )

            translated_message = TranslatedMessage(
                {self.ctx.default_language: cleaned_key}
            )

            for shortcode, message in message_dict.items():
                if shortcode not in self.__enabled_languages:
                    continue

                var_token, var_name = p_util.get_var_token_and_name(message)
                translated_message.messages[shortcode] = message

                if var_token:
                    translated_message.variables[var_token] = var_name

            self.__key_message_dict[cleaned_key] = copy.deepcopy(translated_message)

    def __message_key_exists(self, message_key: str) -> None:
        """Raises TranslationException if message key is not associated with translated message."""
        if message_key not in self.__key_message_dict:
            raise TranslationException(
                f"""
                    Key: {message_key} does not exist.
                    Make sure that {message_key} exists in the translations
                """
            )
