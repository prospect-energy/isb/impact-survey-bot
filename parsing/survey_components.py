"""Contains component factory to validate completeness and type_check json dicts."""
from inspect import isclass
from typing import Any, Dict, List, Optional, Tuple, Union

import parsing.util as p_util
from result.error import (
    ComponentException,
    EmptyValueException,
    InvalidTypeException,
    MandatoryException,
)


class Component:
    """Class inherited by all format-checking structs."""


class RawConfigStruct(Component):
    """Class to parse config section of survey."""

    def __init__(self, json_dict: dict):
        interface = {
            "create-db-entry": bool,
            "table": Optional[str],
            "id-column": Optional[str],
            "has-finish-message": bool,
            "finish-message": Optional[str],
        }

        json_dict = parse_survey_component(interface, json_dict)

        self.create_db_entry = json_dict.get("create-db-entry", None)
        self.table = json_dict.get("table", None)
        self.id_column = json_dict.get("id-column", None)
        self.has_finish_message = json_dict.get("has-finish-message", None)
        self.finish_message = json_dict.get("finish-message", None)

        if self.create_db_entry is True and (not self.table or not self.id_column):
            raise MandatoryException(
                "If create-db-entry is enabled, the fields table and id-column are required.",
            )

        if self.has_finish_message is True and not self.finish_message:
            raise MandatoryException(
                "If has-finish-message is enabled, the field finish-message is required."
            )


class RawConditionStruct(Component):
    """Class to parse condition section of step."""

    def __init__(self, json_dict: dict):
        interface = {
            "operator": str,
            "value": Union[str, int, float, list],
            "compare-with": Optional[Any],
            "compare-range": Optional[
                Tuple[Union[str, int, float], Union[str, int, float]]
            ],
            "jump-to": str,
        }

        json_dict = parse_survey_component(interface, json_dict)

        self.operator = json_dict.get("operator", None)
        self.value = json_dict.get("value", None)
        self.compare_with = json_dict.get("compare-with", None)
        self.compare_range = json_dict.get("compare-range", None)
        self.jump_to = json_dict.get("jump-to", None)

        if not self.compare_with and not self.compare_range:
            raise MandatoryException(
                "One of 'compare-with' or 'compare-range' is mandatory."
            )


class RawSkipStruct(Component):
    """Class to parse skip section of step."""

    def __init__(self, json_dict: dict):
        interface = {"enabled": bool, "jump-to": str}

        json_dict = parse_survey_component(interface, json_dict)

        self.enabled = json_dict.get("enabled", None)
        self.jump_to = json_dict.get("jump-to", None)


class RawFileUploadStruct(Component):
    """Class to parse file-upload section of step."""

    def __init__(self, json_dict: dict):
        interface = {"name": str}

        json_dict = parse_survey_component(interface, json_dict)

        self.name = json_dict.get("name", None)


class RawCompareStruct(Component):
    """Class to parse compare section of step."""

    def __init__(self, json_dict: dict):
        interface = {
            "operator": str,
            "compare-with": Optional[Any],
            "compare-range": Optional[
                Tuple[Union[int, float, str], Union[int, float, str]]
            ],
            "fail": str,
        }

        json_dict = parse_survey_component(interface, json_dict)

        self.operator = json_dict.get("operator", None)
        self.compare_with = json_dict.get("compare-with", None)
        self.compare_range = json_dict.get("compare-range", None)
        self.fail = json_dict.get("fail", None)

        if not self.compare_with and not self.compare_range:
            raise MandatoryException(
                "One of 'compare-with' or 'compare-range' is mandatory."
            )


class RawSpecifyStruct(Component):
    """Class to parse please-specify section of step."""

    def __init__(self, json_dict: dict):
        interface = {
            "option": str,
            "message": str,
            "successor": str,
            "input-type": str,
            "modify": Optional[list],
            "help": Optional[str],
            "image": Optional[str],
        }

        json_dict = parse_survey_component(interface, json_dict)

        self.option = json_dict.get("option", None)
        self.message = json_dict.get("message", None)
        self.successor = json_dict.get("successor", None)
        self.input_type = json_dict.get("input-type", None)
        self.modify = json_dict.get("modify", None)
        self.help_message = json_dict.get("help", None)
        self.image = json_dict.get("image", None)


class RawStep(Component):
    """Class to parse id and type in step json dictionary."""

    def __init__(self, json_dict: dict):
        interface = {"id": str, "type": str}

        json_dict = parse_survey_component(interface, json_dict)

        self.id_ = json_dict.get("id", "")
        self.type = json_dict.get("type", "").lower()


class RawMessageStep(Component):
    """Class to parse message step json dictionary."""

    def __init__(self, json_dict: dict):
        interface = {
            "id": str,
            "message": str,
            "help": Optional[str],
            "image": Optional[str],
            "help-image": Optional[str],
            "input-type": str,
            "modify": Optional[list],
            "skip": Optional[RawSkipStruct],
            "condition": Optional[RawConditionStruct],
            "compare": Optional[RawCompareStruct],
            "file-upload": Optional[RawFileUploadStruct],
            "db-column": Optional[str],
            "save-to-db-directly": Optional[bool],
            "successor": str,
        }

        json_dict = parse_survey_component(interface, json_dict)

        self.id_ = json_dict.get("id", None)
        self.message = json_dict.get("message", None)
        self.help_message = json_dict.get("help", None)
        self.image = json_dict.get("image", None)
        self.help_image = json_dict.get("help-image", None)
        self.input_type = json_dict.get("input-type", None)
        self.modifiers = json_dict.get("modify", None)
        self.skip = json_dict.get("skip", None)
        self.condition = json_dict.get("condition", None)
        self.compare = json_dict.get("compare", None)
        self.file_upload = json_dict.get("file-upload", None)
        self.db_column = json_dict.get("db-column", None)
        self.save_to_db_directly = json_dict.get("save-to-db-directly", None)
        self.successor = json_dict.get("successor", None)


class RawActionStep(Component):
    """Class to parse action step json dictionary."""

    def __init__(self, json_dict: dict):
        interface = {
            "id": str,
            "function": str,
            "params": dict,
            "condition": Optional[RawConditionStruct],
            "successors": dict,
        }

        json_dict = parse_survey_component(interface, json_dict)

        self.id_ = json_dict.get("id", None)
        self.function = json_dict.get("function", None)
        self.params = json_dict.get("params", None)
        self.condition = json_dict.get("condition", None)
        self.successors = json_dict.get("successors", None)


class RawSelectionStep(Component):
    """Class to parse selection step json dictionary."""

    def __init__(self, json_dict: dict):
        interface = {
            "id": str,
            "message": str,
            "help": Optional[str],
            "image": Optional[str],
            "help-image": Optional[str],
            "modify": Optional[list],
            "multiple-choice": Optional[bool],
            "options": List[Tuple[str, str, Optional[Any], Optional[str]]],
            "skip": Optional[RawSkipStruct],
            "condition": Optional[RawConditionStruct],
            "please-specify": Optional[RawSpecifyStruct],
            "db-column": Optional[str],
            "save-to-db-directly": Optional[bool],
        }

        json_dict = parse_survey_component(interface, json_dict)

        self.id_ = json_dict.get("id", None)
        self.message = json_dict.get("message", None)
        self.help_message = json_dict.get("help", None)
        self.image = json_dict.get("image", None)
        self.help_image = json_dict.get("help-image", None)
        self.modifiers = json_dict.get("modify", None)
        self.multiple_choice = json_dict.get("multiple-choice", None)
        self.options = json_dict.get("options", None)
        self.skip = json_dict.get("skip", None)
        self.condition = json_dict.get("condition", None)
        self.please_specify = json_dict.get("please-specify", None)
        self.db_column = json_dict.get("db-column", None)
        self.save_to_db_directly = json_dict.get("save-to-db-directly", None)


class RawSurveyStruct(Component):
    """Class to parse survey json dictionary."""

    def __init__(self, json_dict: dict) -> None:
        interface = {"config": RawConfigStruct, "steps": list}

        json_dict = parse_survey_component(interface, json_dict)

        self.config = json_dict.get("config", None)
        self.steps = json_dict.get("steps", None)


def is_optional(type_: Any) -> bool:
    """Returns whether type_ is optional type."""
    if hasattr(type_, "__origin__"):
        args = type_.__args__
        if type_.__origin__ == Union and args[len(args) - 1] == type(None):
            return True

    return False


def is_dict(value: Any) -> bool:
    """Returns whether value is instance of dictionary."""
    return isinstance(value, dict)


def get_type_name(type_: Any) -> str:
    """Returns stringified type."""
    return str(type_)


def is_typing_type(type_: Any) -> bool:
    """Returns whether type_ is a type-hint of typing module."""
    return hasattr(type_, "__origin__")


def is_typing_compound_type(type_: Any) -> bool:
    """Returns whether type_ is a compound type-hint of typing module
    (e.g. Optional, Union, Tuple, List, Dict, Set)"""
    return is_typing_type(type_) and hasattr(type_, "__args__")


def is_typing_tuple(type_: Any) -> bool:
    """Returns whether type_ is a Tuple class."""
    return is_typing_type(type_) and type_.__origin__ == tuple


def is_typing_list(type_: Any) -> bool:
    """Returns whether type_ is a List class."""
    return is_typing_type(type_) and type_.__origin__ == list


def is_typing_union(type_: Any) -> bool:
    """Returns whether type_ is a Union class."""
    return is_typing_type(type_) and type_.__origin__ == Union


def is_component_class(type_: Any) -> bool:
    """Returns whether type_ is subclass of Component."""
    return isclass(type_) and issubclass(type_, Component)


def get_optional_type(type_: Any) -> Any:
    """Returns the inner type of Optional[]."""
    if len(type_.__args__) == 2:  # Optional[ not Union ]
        type_ = type_.__args__[0]
    else:  # Optional[ Union ]
        type_.__args__ = type_.__args__[:-1]

    return type_


def is_compatible_type(value: Any, expected_type: Any) -> bool:
    """Returns whether value has compatible type to expected_type.
    Ignores optional flag.
    """
    value_type = type(value)

    if is_optional(expected_type):
        expected_type = get_optional_type(expected_type)

    if value_type == expected_type or expected_type == Any:
        return True

    if is_typing_type(expected_type):
        origin = expected_type.__origin__
        if value_type == origin:
            return True
        if origin == Union:
            return value_type in expected_type.__args__

    return False


def verify_tuple(key: str, value: list, expected_type: Tuple) -> None:
    """Verifies that items in list match the lenght and positional types of the expected tuple."""
    expected_items = expected_type.__args__

    if not isinstance(value, list):
        raise InvalidTypeException(
            f"""Value of key: {key} expects a list with types:
            {",".join([get_type_name(type_) for type_ in expected_items])}."""
        )

    if len(value) > len(expected_items):
        raise InvalidTypeException(
            f"Value of key: {key} expects a list of length {len(expected_type.__args__)}"
        )

    for idx, positional_type in enumerate(expected_items):
        positional_value = p_util.safe_get(value, idx, None)

        if is_optional(positional_type) and positional_value is None:
            continue

        if not is_compatible_type(positional_value, positional_type):
            raise InvalidTypeException(
                f"""Value of key: {key} expects a list with type
                {get_type_name(positional_type)} at position {idx+1}."""
            )


def verify_list(key: str, value: list, expected_type: List) -> None:
    """Verifies that items in list match the expected item type."""
    expected_item_type = expected_type.__args__[0]

    if not isinstance(value, list):
        raise InvalidTypeException(
            f"Value of key: {key} expects a list with type {get_type_name(expected_item_type)}."
        )

    if is_typing_tuple(expected_item_type):
        for item in value:
            verify_tuple(key, item, expected_item_type)
            return

    for item in value:
        if not is_compatible_type(item, expected_item_type):
            raise InvalidTypeException(
                f"""Value of key: {key} expects a list with items of type:
                {get_type_name(expected_item_type)}."""
            )


def get_type_checked_value(key: str, value: Any, expected_type: Any) -> Any:
    """Wrapper function for doing the type-compability check."""
    optional = is_optional(expected_type)

    if optional:
        expected_type = get_optional_type(expected_type)

    if is_component_class(expected_type):
        if is_dict(value):
            return expected_type(value)  # executing __init__() of struct class

        raise InvalidTypeException(
            f"""Value of key: {key} has invalid type, expected a dictionary with certain structure."""
        )

    if is_typing_tuple(expected_type):
        verify_tuple(key, value, expected_type)
        return value

    if is_typing_list(expected_type):
        verify_list(key, value, expected_type)
        return value

    if not is_compatible_type(value, expected_type):
        if is_typing_union(expected_type):
            raise InvalidTypeException(
                f"""Value of key: {key} has invalid type, expected:
            {' or '.join([get_type_name(t) for t in expected_type.__args__])}"""
            )

        raise InvalidTypeException(
            f"Value of key: {key} has invalid type, expected: {get_type_name(expected_type)}"
        )

    if expected_type == str and not optional and value == "":
        raise EmptyValueException(f"Field {key} has empty value.")

    return value


def parse_survey_component(interface: dict, json_dict: dict) -> Dict[str, Any]:
    """Parses and validate types of json dict."""
    return_dict = {}

    if not is_dict(json_dict):
        raise ComponentException("Object is not a dictionary")

    available_keys = json_dict.keys() & interface.keys()
    missing_keys = interface.keys() - json_dict.keys()

    for key in missing_keys:
        expected_type = interface[key]

        if not is_optional(expected_type):
            raise MandatoryException(
                f"Mandatory field {key} is missing.", {"required": key}
            )

    for key in available_keys:
        value = json_dict[key]
        expected_type = interface[key]

        value = get_type_checked_value(key, value, expected_type)
        return_dict[key] = value

    return return_dict
