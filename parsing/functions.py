"""Contains all survey functions.
Functions are sorted alphabetically.

Typing hints in this module are used for custom type-checking.
Please don't update type-hints unless you understand how the
survey validation works!
"""
import json
import re
from typing import Any

import requests
from psycopg2 import DatabaseError

from database.components import Column, OrderBy, Table, WhereClause
from database.queries import get_participant_entry_by_verification_code, is_empty
from database.serializer import default_json_serializer
from parsing import comparison, math
from result.error import DatabaseException
from result.status import NotSet, StatusCode
from structs.context import SessionContext
from structs.messages import Media, MediaOrigin
from structs.selection import SelectedItemsList, SelectionOption
from survey.util import get_variable_value
from type_check import request_validation
from type_check.datetime import can_be_date, can_be_time, strp_date, strp_time
from type_check.input_types import Url


def add_numbers(value1: Any, value2: Any, **_):
    """More information: /docs/PROGRAMMING.md#add-numbers"""

    return math.add(value1, value2)


def append_to_database_value(
    ctx: SessionContext,
    table: str,
    where_column: str,
    where_value: Any,
    column: str,
    value: Any,
    delimiter: str,
    **_,
) -> StatusCode:
    """More information: /docs/PROGRAMMING.md#append-to-database-value"""

    table = Table(ctx.workspace_schema, str(table))
    where_column = Column(table, str(where_column))
    column = Column(table, str(column))
    delimiter = str(delimiter)

    try:
        row = ctx.postgres.select(
            table=table,
            all_columns=False,
            columns=[column],
            joined_tables=None,
            where_clauses=[WhereClause(where_column, where_value)],
        )
        if row == []:
            ctx.logger.error(
                f"""
                    [append_to_db_value] Row is empty list for:
                    {table}, column {column},where_value: {where_value}
                """
            )
            return StatusCode.FAIL

        new_db_value = ""

        if row:
            new_db_value = f"{row}{delimiter}{value}"
        else:
            new_db_value = value

        table_obj = Table(ctx.workspace_schema, table)
        ctx.postgres.update(
            Table(ctx.workspace_schema, table),
            [WhereClause(Column(table_obj, where_column), where_value)],
            {column: new_db_value},
        )
    except DatabaseError as error:
        ctx.logger.error(f"[append_to_db_value] {error}")
        return StatusCode.FAIL

    return StatusCode.SUCCESS


def can_be_float(value: Any, **_) -> bool:
    """More information: /docs/PROGRAMMING.md#can-be-float"""

    return request_validation.can_be_float_str(str(value))


def can_be_int(value: Any, **_) -> bool:
    """Checks if value can be integer.
    Function does not change type of value.

    Params:
        - value: Value that is checked.
    """

    return request_validation.can_be_int_str(str(value))


def check_verification_code(ctx: SessionContext, verification_code: str, **_) -> Any:
    """Checks status of provided verification code.
    Only used by registration survey.

    Params:
        - input_list: List to be compared.
        - target_list: List to be compared.

    Returns:
        - "invalid": Verification Code does not exist.
        - "used": Verification Code is used already.
        - "available": Verification Code is unclaimed.
    """

    try:
        row = get_participant_entry_by_verification_code(
            ctx.postgres, verification_code
        )
    except DatabaseException as error:
        ctx.logger.error(f"[check_verification_code] {error}")
        return StatusCode.FAIL

    if row == [] or row is None:
        return "invalid"

    participant_identifier = row[0]

    if participant_identifier:
        return "used"

    return "available"


def clear_database_cache(ctx: SessionContext, **_) -> StatusCode:
    """Clears database cache.
    Items in database cache are not saved to database automatically.
    """

    ctx.session.db_cache = {}

    return StatusCode.SUCCESS


def database_contains(
    ctx: SessionContext, table: str, where_column: str, where_value: Any, **_
) -> bool:
    """More information: /docs/PROGRAMMING.md#database-contains"""

    table = Table(ctx.workspace_schema, str(table))
    where_column = Column(table, str(where_column))

    row = None
    try:
        row = ctx.postgres.select(
            table=table,
            all_columns=False,
            columns=[where_column],
            joined_tables=None,
            where_clauses=[WhereClause(where_column, where_value)],
        )
    except DatabaseError as error:
        ctx.logger.error(f"[database_contains] {error}")

    return not (row is None or row == [])


def devide_numbers(value1: Any, value2: Any, **_):
    """More information: /docs/PROGRAMMING.md#devide-numbers"""

    return math.devide(value1, value2)


def event(ctx: SessionContext, event_name: str, data: dict, **_) -> StatusCode:
    """More information: /docs/PROGRAMMING.md#event"""
    event_name = str(event_name)

    return ctx.event_handler.trigger(event_name, data)


def forward_to_step(**_) -> bool:
    """Returns true. Can be used to forward immediately to next step."""
    return True


def get_database_value(
    ctx: SessionContext,
    table: str,
    where_column: str,
    where_value: Any,
    column: str,
    **_,
) -> Any:
    """More information: /docs/PROGRAMMING.md#get-database-value"""

    table = Table(ctx.workspace_schema, str(table))
    where_column = Column(table, str(where_column))
    column = Column(table, str(column))

    try:
        row = ctx.postgres.select(
            table=table,
            all_columns=False,
            columns=[column],
            joined_tables=None,
            where_clauses=[WhereClause(where_column, where_value)],
        )
    except DatabaseError as error:
        ctx.logger.error(f"[get_database_value] {error}")
        return StatusCode.FAIL

    if row is None or row == []:
        ctx.logger.debug(
            f"""
                [get_database_value] Row is empty for table:
                {table}, column: {column}, id: {where_value}
            """
        )
        return StatusCode.FAIL

    return row


def get_database_value_by_order(
    ctx: SessionContext,
    table: str,
    where_column: str,
    where_value: Any,
    column: str,
    sort_by: str,
    order_direction: str,
    **_,
) -> Any:
    """More information: /docs/PROGRAMMING.md#get-database-value-by-order"""

    table = Table(ctx.workspace_schema, str(table))
    where_column = Column(table, str(where_column))
    column = Column(table, str(column))
    sort_by = Column(table, str(sort_by))
    order_direction = str(order_direction)

    if order_direction.lower() != "asc" and order_direction.lower() != "desc":
        ctx.logger.error(
            f"[get_database_value_by_order] Invalid order direction: {order_direction}"
        )
        return StatusCode.FAIL

    try:
        row = ctx.postgres.select(
            table=table,
            all_columns=False,
            columns=[column],
            joined_tables=None,
            where_clauses=[WhereClause(where_column, where_value)],
            order_by=OrderBy(sort_by, order_direction),
        )
    except DatabaseError as error:
        ctx.logger.error(f"[get_database_value_by_order] {error}")
        return StatusCode.FAIL

    if row is None or row == []:
        ctx.logger.debug(
            f"""
                [get_database_value_by_order] Row is empty for table:
                {table}, column: {column}, id: {where_value}
            """
        )
        return StatusCode.FAIL

    return row


def get_first_match(
    ctx: SessionContext, input_list: list, target_items: list, **_
) -> Any:
    """More information: /docs/PROGRAMMING.md#get-first-match"""

    if not isinstance(input_list, list):
        ctx.logger.debug(
            f"[get_first_match] Failed because input_list is not a list: {input_list}"
        )
        return "default"

    for first_item in input_list:
        for second_item in target_items:
            if first_item == second_item:
                return first_item

    ctx.logger.debug(
        f"""
            [get_first_match] Failed because no match - input_list:
            {str(input_list)}, target_items: {str(target_items)}
        """
    )
    return "default"


def has_format(ctx: SessionContext, compare_with: list, value: Any, **_) -> bool:
    """More information: /docs/PROGRAMMING.md#has-format"""

    if not isinstance(compare_with, list):
        ctx.logger.error(
            '[check_formats] compare_with must be a list of regex patterns: ["..."]'
        )
        return False

    for item in compare_with:
        pattern = str(item)

        if re.match(pattern, str(value)):
            return True

    return False


def has_groups(compare_with: list, value: list, **_) -> bool:
    """Checks is participant is part of one or multiple groups.
    Mainly used by condition and selection steps.

    Params:
        - compare_with: List of groups (e.g. {{ session.groups }}).
        - value: List of group names that should be contained.
    """

    if not isinstance(compare_with, list):
        compare_with = [str(compare_with)]

    if not isinstance(value, list):
        value = [str(value)]

    for group in value:
        if group not in compare_with:
            return False

    return True


def has_selected(compare_with: str, value: Any, **_) -> bool:
    """Checks is compare_with contains value.
    Mainly used by condition and selection steps.

    Params:
        - compare_with: Comma-Seperated string.
        - value: Value that is searched for.
    """

    if "," in str(compare_with):
        compare_with = str(compare_with).split(",")
    else:
        compare_with = [compare_with]

    if isinstance(value, list):
        for item in value:
            if item in compare_with:
                return True

    return value in compare_with


def is_between(lower_bound: Any, upper_bound: Any, value: Any, **_) -> bool:
    """More information: /docs/PROGRAMMING.md#is-between"""

    if (
        isinstance(lower_bound, NotSet)
        or isinstance(upper_bound, NotSet)
        or isinstance(value, NotSet)
    ):
        return True

    return comparison.between(value, lower_bound, upper_bound)


def is_between_including(lower_bound: Any, upper_bound: Any, value: Any, **_) -> bool:
    """More information: /docs/PROGRAMMING.md#is-between-including"""

    if (
        isinstance(lower_bound, NotSet)
        or isinstance(upper_bound, NotSet)
        or isinstance(value, NotSet)
    ):
        return True

    return comparison.between_including(value, lower_bound, upper_bound)


def is_database_field_empty(
    ctx: SessionContext,
    table: str,
    where_column: str,
    where_value: Any,
    column: str,
    **_,
) -> bool:
    """More information: /docs/PROGRAMMING.md#is-database-field-empty"""

    table = Table(ctx.workspace_schema, str(table))
    where_column = str(where_column)
    column = str(column)

    try:
        is_empty = is_empty(ctx.postgres, table, where_column, where_value, column)
    except DatabaseError as error:
        ctx.logger.error(f"[database_field_is_empty] {error}")
        return False

    return is_empty


def is_equal(compare_with: Any, value: Any, **_) -> bool:
    """More information: /docs/PROGRAMMING.md#is-equal"""

    if isinstance(compare_with, NotSet) or isinstance(value, NotSet):
        return True

    return value == compare_with


def is_greater_or_equal_than(compare_with: Any, value: Any, **_) -> bool:
    """More information: /docs/PROGRAMMING.md#is-greater-or-equal"""

    if isinstance(compare_with, NotSet) or isinstance(value, NotSet):
        return True

    return comparison.greater_or_equal(value, compare_with)


def is_greater_than(compare_with: Any, value: Any, **_) -> bool:
    """More information: /docs/PROGRAMMING.md#is-greater-than"""

    if isinstance(compare_with, NotSet) or isinstance(value, NotSet):
        return True

    return comparison.greater(value, compare_with)


def is_greater_time_including_midnight(compare_with: Any, value: Any, **_) -> bool:
    """More information: /docs/PROGRAMMING.md#is-later-time-including-midnight"""

    if isinstance(compare_with, NotSet) or isinstance(value, NotSet):
        return True

    return comparison.greater_midnight(value, compare_with)


def is_less_or_equal_than(compare_with: Any, value: Any, **_) -> bool:
    """More information: /docs/PROGRAMMING.md#is-less-or-equal"""

    if isinstance(compare_with, NotSet) or isinstance(value, NotSet):
        return True

    return comparison.less_or_equal(value, compare_with)


def is_less_than(compare_with: Any, value: Any, **_) -> bool:
    """More information: /docs/PROGRAMMING.md#is-less-than"""

    if isinstance(compare_with, NotSet) or isinstance(value, NotSet):
        return True

    return comparison.less(value, compare_with)


def multiply_numbers(value1: Any, value2: Any, **_):
    """More information: /docs/PROGRAMMING.md#multiply-numbers"""

    return math.multiply(value1, value2)


def overwrite_step_variable(
    ctx: SessionContext, id_: str, value: Any, **_
) -> StatusCode:
    """Overwrites value of specific step variable (e.g. step.<id_>).
    Does not update the history.
    Mainly used in support steps for selection.

    Params:
        - id_: Id of step that should be updated.
        - value: New value of step variable.
    """

    ctx.session.survey_variables[id_] = value
    return StatusCode.SUCCESS


def replace_in_text(text: str, value: str, replace_with: str, **_) -> str:
    """More information: /docs/PROGRAMMING.md#replace-in-text"""

    return str(text).replace(str(value), str(replace_with))


def return_value(value: Any, **_) -> Any:
    """Returns provided value.
    Can be used for defining different successors based on value.

    Params:
        - value: Any value.
    """

    return value


def selection_action(ctx: SessionContext, options: dict, multiple_choice: bool, **_):
    """Returns a list of selected items based on participant input.
    Only used by selection step directly.

    Params:
        - options: Selection options in the format {<group>: [SelectionOption]}
        - multiple_choice: Is multiple-choice enabled?
    """

    session_groups = ctx.session.groups
    participant_input = ctx.session.variables.get("request.input")

    visible_options = []
    matching_selection_groups = [
        group for group in options.keys() if group in session_groups
    ]

    for group in matching_selection_groups:
        o_list = options.get(group, [])
        for option in o_list:
            visible_options.append(option)

    participant_input = participant_input.strip()

    participant_input_list = []

    if multiple_choice:
        participant_input = participant_input.replace(".", ",")
        participant_input = participant_input.split(",")

        for item in participant_input:
            item = item.strip()
            if item not in participant_input_list:
                participant_input_list.append(item)
    else:
        participant_input = participant_input.replace(".", "")
        participant_input = participant_input.replace(",", "")

        participant_input_list = [participant_input]

    selected_items = []
    for item in participant_input_list:
        selected_number = 0
        try:
            selected_number = int(item)
        except ValueError:
            return StatusCode.INVALID

        if selected_number > len(visible_options) or selected_number <= 0:
            return StatusCode.INVALID

        option: SelectionOption = visible_options[selected_number - 1]
        if option.output_is_variable:
            option.output = get_variable_value(
                ctx.session.get_all_variables(),
                option.output,
                option.output,
                option.output,
            )

        selected_items.append(option)

    return SelectedItemsList(selected_items)


def set_custom_field(
    ctx: SessionContext,
    field_name: str,
    field_value: Any,
    **_,
) -> StatusCode:
    """Updates the custom_fields dictionary for session.
    Only used within registration survey.

    Params:
        - field_name: Name of custom field.
        - field_value: Participant's value for custom field.
    """

    field_name = str(field_name)

    ctx.session.custom_fields[field_name] = field_value
    return StatusCode.SUCCESS


def set_database_value(
    ctx: SessionContext,
    table: str,
    where_column: str,
    where_value: Any,
    column: str,
    value: Any,
    **_,
) -> StatusCode:
    """More information: /docs/PROGRAMMING.md#set-database-value"""

    table = Table(ctx.workspace_schema, str(table))
    where_column = str(where_column)
    column = str(column)

    if can_be_date(value):
        value = strp_date(value)
    elif can_be_time(value):
        value = strp_time(value)

    try:
        ctx.postgres.update(
            table,
            [WhereClause(Column(table, where_column), where_value)],
            {column: value},
        )
    except DatabaseError as error:
        ctx.logger.error(f"[set_database_value] {error}")
        return StatusCode.FAIL

    return StatusCode.SUCCESS


def set_language(ctx: SessionContext, language: str, **_) -> StatusCode:
    """Sets language of session.
    Language must be a valid language shortcode - no check!

    Params:
        - language: Language shortcode.
    """

    language = str(language)

    ctx.session.set_language(ctx.postgres, language)
    return StatusCode.SUCCESS


def set_variable(
    ctx: SessionContext,
    var_name: str,
    var_value: Any,
    **_,
) -> StatusCode:
    """More information: /docs/PROGRAMMING.md#set-variable"""

    var_name = str(var_name)

    ctx.session.survey_variables[f"var.{var_name}"] = var_value
    return StatusCode.SUCCESS


def substract_numbers(value1: Any, value2: Any, **_):
    """More information: /docs/PROGRAMMING.md#substract-numbers"""

    return math.substract(value1, value2)


def update_database_entry(
    ctx: SessionContext,
    table: str,
    where_column: str,
    where_value: Any,
    data: dict,
    **_,
) -> StatusCode:
    """More information: /docs/PROGRAMMING.md#update-database-entry"""

    table = Table(ctx.workspace_schema, str(table))
    where_column = str(where_column)

    if data == {}:
        ctx.logger.error(
            f"""
                [update_database_entry] Got empty data dictionary for:
                {table}, where_value: {where_value}
            """
        )
        return StatusCode.FAIL

    try:
        ctx.postgres.update(
            table, [WhereClause(Column(table, where_column), where_value)], data
        )
    except DatabaseError as error:
        ctx.logger.error(f"[update_database_entry] {error}")
        return StatusCode.FAIL

    return StatusCode.SUCCESS


def update_database_cache(ctx: SessionContext, data: dict, **_) -> StatusCode:
    """More information: /docs/PROGRAMMING.md#set-cached-database-value"""

    for column, value in data.items():
        column = str(column)

        if can_be_date(value):
            value = strp_date(value)
        elif can_be_time(value):
            value = strp_time(value)

        ctx.session.db_cache[column] = value

    return StatusCode.SUCCESS


def update_session(ctx: SessionContext, **_) -> StatusCode:
    """Restarts the session.
    Therefore triggers an reload of the participant data and
    the reset of all attributes and variables.
    """

    ctx.session.start()
    return StatusCode.SUCCESS


def upload_registration_inputs(ctx: SessionContext, participant_id: str) -> StatusCode:
    """Convenience function to upload registration inputs.
    Uses database cache values and participant_id as id value.
    Only used by registration survey.

    Params:
        - participant_id: Id of participant that completed registration.
    """

    table = Table(ctx.postgres.shared_schema, "participants")

    session = ctx.session
    session.db_cache["custom_fields"] = json.dumps(
        session.custom_fields, default=default_json_serializer
    )

    for key, value in session.db_cache.items():
        if isinstance(value, NotSet):
            session.db_cache[key] = None

    try:
        ctx.postgres.update(
            table,
            [WhereClause(Column(table, "participant_id"), int(participant_id))],
            session.db_cache,
        )
    except DatabaseError as error:
        ctx.logger.error(f"[upload_registration_inputs] {error}")
        return StatusCode.FAIL

    session.db_cache = {}

    return StatusCode.SUCCESS


def upload_media(ctx: SessionContext, media: Media, **_) -> StatusCode:
    """More information: /docs/PROGRAMMING.md#upload-media"""

    url = media.url
    headers = {}

    if media.origin == MediaOrigin.WHATSAPP_BUSINESS:
        media_retrival_url = f"https://graph.facebook.com/v15.0/{media.file_id}"
        headers = {"Authorization": f"Bearer {ctx.wab_api_token}"}
        response = requests.get(media_retrival_url, headers=headers).json()
        url = response["url"]

    if media.origin == MediaOrigin.TELEGRAM:
        get_file_url = f"https://api.telegram.org/bot{ctx.telegram_api_token}/getFile"
        file_object = (
            requests.post(get_file_url, data={"file_id": media.file_id})
            .json()
            .get("result", {})
        )
        file_path = file_object.get("file_path")
        if not file_path:
            return StatusCode.FAIL
        url = f"https://api.telegram.org/file/bot{ctx.telegram_api_token}/{file_path}"

    return upload_file_to_s3(ctx, url, headers)


def upload_file_to_s3(ctx: SessionContext, url: Url, headers: dict, **_) -> StatusCode:
    """More information: /docs/PROGRAMMING.md#upload-file-to-s3"""

    url = str(url)

    try:
        file_url = ctx.s3.upload_file_from_url(url, headers, ctx.workspace_asset_key)
    except DatabaseException as error:
        ctx.logger.error(f"[upload_file_to_s3] {error}")
        return StatusCode.FAIL

    return file_url


def value_in_list(list_: Any, value: Any, **_) -> bool:
    """More information: /docs/PROGRAMMING.md#value-in-list"""

    if type(list_) not in [str, list]:
        return False

    if isinstance(list_, str):
        list_ = list_.split(",")

    for item in list_:
        if value == str(item):
            return True

    return False
