"""Contains step classes as well as support structs and functions."""
import copy
from typing import Any, Callable, Dict, List, Optional

from result.status import StatusCode
from structs.context import SessionContext
from structs.messages import IncommingMessage, ResponseMessage, TranslatedMessage
from structs.sections import (
    CompareSection,
    ConditionSection,
    SkipSection,
    SpecifySection,
)
from structs.selection import SelectedItemsList, SelectionOption
from survey import util


class ActionOutput:
    """Struct for storing data about the output of action step."""

    def __init__(self, successor_id: Optional[str], return_value: Any) -> None:
        self.successor_id = successor_id
        self.return_value = return_value


class Step:
    """Class for storing general step data."""

    def __init__(
        self, id_: str, type_: str, condition: Optional[ConditionSection]
    ) -> None:
        self.id_ = id_
        self.type = type_
        self.condition = condition

    def condition_fulfilled(self, ctx: SessionContext) -> bool:
        """Returns if condition for displaying step is fulfilled."""
        return condition_fulfilled(ctx, self.condition)


class ResponseStep(Step):
    """Class for storing data of response step (message or selection)."""

    def __init__(
        self,
        id_: str,
        type_: str,
        condition: Optional[ConditionSection],
        variable_id: str,
        message: str,
        image: Optional[str],
        help_: Optional[TranslatedMessage],
        help_image: Optional[str],
        skip: Optional[SkipSection],
        db_column: Optional[str],
        successor: Optional[str],
    ) -> None:

        super().__init__(id_, type_, condition)

        self.variable_id = variable_id
        self.message = message
        self.image = image
        self.help_ = help_
        self.help_image = help_image
        self.skip = skip
        self.db_column = db_column
        self.successor = successor

    def get_message(self, ctx: SessionContext) -> Optional[ResponseMessage]:
        """Returns response message object for message of message step."""
        return build_message_response(ctx, self.message, self.image)

    def get_help(self, ctx: SessionContext) -> Optional[ResponseMessage]:
        """Returns response message object for help message of message step."""
        return build_message_response(ctx, self.help_, self.help_image)


class MessageStep(ResponseStep):
    """Class for storing data of message step."""

    def __init__(
        self,
        id_: str,
        variable_id: str,
        message: TranslatedMessage,
        modifiers: List[Callable],
        input_validation: Callable,
        input_type: str,
        invalid_input_message: TranslatedMessage,
        successor: Optional[str] = None,
        image: Optional[str] = None,
        help_: Optional[TranslatedMessage] = None,
        help_image: Optional[str] = None,
        skip: Optional[SkipSection] = None,
        condition: Optional[ConditionSection] = None,
        compare: Optional[CompareSection] = None,
        db_column: Optional[str] = None,
    ) -> None:
        super().__init__(
            id_=id_,
            type_="message",
            condition=condition,
            variable_id=variable_id,
            message=message,
            image=image,
            help_=help_,
            help_image=help_image,
            skip=skip,
            db_column=db_column,
            successor=successor,
        )

        self.modifiers = modifiers
        self.input_validation = input_validation
        self.input_type = input_type
        self.invalid_input_message = invalid_input_message
        self.compare = compare
        self.successor = successor

    def get_invalid_input(self, ctx: SessionContext) -> ResponseMessage:
        """Returns response message object for invalid-input message."""
        return self.invalid_input_message.to_response(ctx, ctx.session.language)

    def modify_input(self, msg: IncommingMessage) -> IncommingMessage:
        """Modifies and returns message based on defined modifiers."""
        for modifier_func in self.modifiers:
            msg = modifier_func(msg)

        return msg

    def validate_input_type(self, msg: IncommingMessage) -> bool:
        """Returns if message matches input type."""
        return self.input_validation(msg)


class ActionStep(Step):
    """Class for storing data of action step."""

    def __init__(
        self,
        id_: str,
        is_selection_action: bool,
        function: Callable,
        params: Dict[str, Any],
        variables: Dict[str, Any],
        successors: Dict[Any, str],
        condition: Optional[ConditionSection] = None,
    ) -> None:
        super().__init__(id_=id_, type_="action", condition=condition)

        self.is_selection_action = is_selection_action
        self.function = function
        self.params = params
        self.variables = variables
        self.successors = successors

    def execute(self, ctx: SessionContext) -> ActionOutput:
        """Execute action step function using action step params."""

        params = resolve_variables_in_params(ctx, self.params, self.variables)

        return_value = self.function(ctx=ctx, **params)

        if isinstance(return_value, StatusCode):
            return_value = return_value.value

        if str(return_value) in self.successors.keys():
            return ActionOutput(self.successors[str(return_value)], return_value)
        if isinstance(return_value, SelectedItemsList):
            return ActionOutput(return_value.successor, return_value.output)
        if self.successors.get("*", None):
            return ActionOutput(self.successors["*"], return_value)

        return ActionOutput(None, return_value)


class SelectionStep(ResponseStep):
    """Class for storing data of selection step."""

    def __init__(
        self,
        id_: str,
        message: TranslatedMessage,
        options: Dict[str, List[SelectionOption]],
        successor: Optional[str] = None,
        image: Optional[str] = None,
        help_: Optional[TranslatedMessage] = None,
        help_image: Optional[str] = None,
        skip: Optional[SkipSection] = None,
        condition: Optional[ConditionSection] = None,
        please_specify: Optional[SpecifySection] = None,
        db_column: Optional[str] = None,
    ) -> None:

        super().__init__(
            id_=id_,
            type_="selection",
            condition=condition,
            variable_id=id_,
            message=message,
            image=image,
            help_=help_,
            help_image=help_image,
            skip=skip,
            db_column=db_column,
            successor=successor,
        )

        self.options = options
        self.please_specify = please_specify

    def get_message(self, ctx: SessionContext) -> ResponseMessage:
        """Returns response message object for message of selection step.
        Overwrites method of response step.
        """
        return build_selection_response(ctx, self.message, self.options, self.image)


def build_message_response(
    ctx: SessionContext,
    message: Optional[TranslatedMessage],
    image: Optional[str] = None,
) -> Optional[ResponseMessage]:
    """Returns response message object for message (with optional media)."""

    if not message:
        return None

    session_language = ctx.session.language
    session_variables = ctx.session.get_all_variables()

    text: str = ""

    text = message.get_message(ctx, session_language)
    var_dict = message.variables

    if text == "":
        return ResponseMessage("message", "")

    for token, var_name in var_dict.items():
        if "step." in var_name:
            var_name = var_name.replace("step.", "")
        var_value = session_variables.get(var_name, "")
        text = text.replace(token, str(var_value))

    return ResponseMessage("message", text, image)


def get_available_selection_options(
    options: Dict[str, List[SelectionOption]], session_groups: List[str]
) -> List[SelectionOption]:
    """Returns a list of the available selection options based on participant groups."""
    available_options = []

    matching_selection_groups = [
        group for group in options.keys() if group in session_groups
    ]

    for group in matching_selection_groups:
        options_of_group = options.get(group, [])
        available_options.extend(options_of_group)

    return available_options


def build_selection_response(
    ctx: SessionContext,
    message: TranslatedMessage,
    options: Dict[str, List[SelectionOption]],
    image: Optional[str] = None,
) -> ResponseMessage:
    """Returns response message objectfor message with selection options
    and optional media.
    """

    session_groups = ctx.session.groups
    session_language = ctx.session.language
    session_variables = ctx.session.get_all_variables()

    header_message = message.get_message(ctx, session_language)
    var_dict = message.variables

    if header_message == "":
        return ResponseMessage("message", "")

    for token, var_name in var_dict.items():
        var_value = session_variables.get(var_name, "")
        header_message = header_message.replace(token, var_value)

    visible_options = []
    available_options = get_available_selection_options(options, session_groups)

    for opt in available_options:
        option_translated_message = opt.text
        option_text = option_translated_message.get_message(ctx, session_language)
        visible_options.append(option_text)

    return ResponseMessage("selection", header_message, image, visible_options)


def resolve_variables_in_params(
    ctx: SessionContext, params: Dict[str, Any], variables: Dict[str, Any]
) -> Dict[str, Any]:
    """Resolves variables within action step params. Returns a modified copy of the params."""

    session_variables = ctx.session.get_all_variables()
    params_copy = copy.deepcopy(params)

    for param_key, variable in variables.items():
        if isinstance(variable, list):
            var_value = params[param_key]
            token = variable[0]
            name = variable[1]

            params_copy[param_key] = util.get_variable_value(
                session_variables, var_value, name, token
            )
        else:
            for valdict_key, valdict_var in variable.items():
                var_value = params[param_key][valdict_key]
                token = valdict_var[0]
                name = valdict_var[1]

                params_copy[param_key][valdict_key] = util.get_variable_value(
                    session_variables, var_value, name, token
                )

    return params_copy


def condition_fulfilled(
    ctx: SessionContext, condition: Optional[ConditionSection]
) -> bool:
    """Returns output of condition function. Returns None if condition is None."""
    if condition is None:
        return True

    func = condition.func
    params = resolve_variables_in_params(ctx, condition.params, condition.variables)

    return func(ctx=ctx, **params)
