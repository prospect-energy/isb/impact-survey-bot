"""Contains Survey class and survey-specific functions."""
from datetime import datetime
from typing import Any, Dict, List

from database.queries import insert_workspace_database_entry
from result.status import NotSet, StatusCode
from structs.context import SessionContext
from structs.sections import ConfigSection
from survey.steps import Step


class Survey:
    """Class for storing and managing survey."""

    def __init__(
        self,
        name: str,
        config: ConfigSection,
        db_columns: Dict[str, Any],
        steps: Dict[str, Step],
    ) -> None:
        self.name = name
        self.config = config
        self.steps = steps
        self.db_columns = db_columns

        self.starting_step = self.steps["START"]
        self.db_enabled = bool(self.config.create_db_entry)

        self.custom_variables = get_variables_set_by_survey(self.steps)

    def start_survey(self, ctx: SessionContext) -> None:
        """Function to be executed when session enters survey."""
        session = ctx.session
        session.db_cache = {}

        session.log_survey_interaction("SURVEY_START")
        session.survey_started_timestamp = datetime.now()

        if self.config.create_db_entry:
            session.db_cache["participant_id"] = session.participant_id
            session.db_cache["created_at"] = datetime.now()
            session.db_cache["completed"] = False

    def finish_survey(self, ctx: SessionContext, ended_by_command: bool) -> None:
        """Function to be executed when session leaves survey.
        Includes transition to other survey or session end.
        """
        session = ctx.session

        if not ended_by_command:
            session.db_cache["completed"] = True

        survey_duration = datetime.now() - session.survey_started_timestamp
        session.db_cache["survey_duration"] = survey_duration.total_seconds()

        for key, value in session.db_cache.items():
            if isinstance(value, NotSet):
                session.db_cache[key] = None

        if self.config.create_db_entry and session.db_cache:
            status = insert_workspace_database_entry(
                ctx.postgres,
                ctx.workspace_schema,
                self.config.table,
                self.config.id_column,
                session.db_cache,
            )

            if status == StatusCode.FAIL:
                ctx.logger.error(
                    f"Could not insert survey data for schema: {ctx.workspace_schema}, table: {self.config.table}"
                )

        session.db_cache = {}

        session.log_survey_interaction("SURVEY_END")

        if self.config.has_finish_message:
            return self.config.finish_message.to_response(ctx, session.language)

        return None


def get_variables_set_by_survey(steps: List[Step]) -> List[str]:
    variables_set_by_survey = []

    for step in steps.values():
        if step.type == "action" and step.function.__name__ == "set_variable":
            if var_name := step.params.get("var_name"):
                variables_set_by_survey.append(f"var.{var_name}")

    return variables_set_by_survey
