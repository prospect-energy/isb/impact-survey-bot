# Module: Survey

The survey module contains the `Survey` and `Step` (including child) classes.
Hereby, the `Survey` class contains a dictionary of `Step` objects that belong to this survey.
In addition, the module also contains support functions and methods for actions on the `Survey` and
`Step` objects.

## When to use it?

The `Step` classes are widely used for parsing, templates and testing.
Besides parsing, the `Survey` class can be used to define pre-generated surveys such as the survey selection.
